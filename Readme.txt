Current Urho3d version: 1.7

Engine Modifications:

Urho3D\Source\ThirdParty\DetourCrowd\source\DetourCrowd.h:
Urho3D\Source\ThirdParty\DetourCrowd\source\DetourCrowd.cpp:
	Modified detourcrowd to have more neighbour checks
	Added render interpolation functionality for fixed tickrate

Urho3D\Source\Urho3D\Navigation\CrowdManager.h,
Urho3D\Source\Urho3D\Navigation\CrowdManager.cpp:
	Modified CrowdManager to update on physics tick

Todos:
	Improve combat dramatically. Need to design better charges, more realstic looking less "spongy". 
	Ideas to try: A. All soldiers move to attack (not just front line), unless a friendly soldier is in the way
	              B. Soldiers have a "threshold" by which they decide to start moving back into formation, instead of staying exactly.
				  C. Some implementation of agressive/defensive formation posturing. Might just be fiddling with moveToAttackDistance, above threshold, and other parameters.