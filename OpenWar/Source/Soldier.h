#pragma once

#include <Urho3D/Navigation/CrowdAgent.h>
#include <Urho3D/Scene/Component.h>

struct SoldierModel;
struct SoldierAnimSet;

using namespace Urho3D;

// A soldier
class Soldier : public Component {
	URHO3D_OBJECT(Soldier, Component);

public:
	Soldier(Context* context);
	~Soldier();

	void Spawn(SoldierModel& model, SoldierAnimSet* animSet);
	
	static void RegisterObject(Context* context);

	void SetToAttack(Node* target);
	bool HasCombatTarget() { return combatTarget_ != nullptr; }
	void InitiateCombat(float timeStep);
	void Hurt(Node* attacker);
	void Push(float timeStep, CrowdAgent* attacker, float amount);

	// The desired position and rotation that the coordinator wants the soldier to take
	// The soldier can ignore these, for example when routing or attacking
	Vector3 coordinatorPosition;
	Vector3 coordinatorDirection;
protected:
	/// Handle node being assigned.
	virtual void OnNodeSet(Node* node);
	void UpdateSim(StringHash eventType, VariantMap& eventData);
	void UpdateRender(StringHash eventType, VariantMap& eventData);

private:
	void CheckAttackFinished(float timeStep);
	void AttemptHitTarget(float timeStep, Node* target);
	void MoveToAttackTarget(float timeStep);
	bool IsInAttackRange(CrowdAgent* attackedAgent);

	void HandleMovementAnimation(float timeStep);
	void Kill();

	Node* combatTarget_ = nullptr;

	SoldierAnimSet* animSet_;
	CrowdAgent* agent_;
	float currSpeed_;

	bool isCurrentlyAttacking_ = false;
	float timeUntilNextAttackAttempt_ = 0.0f;
	float attackAnimTimeLeft_ = 0.0f;
};