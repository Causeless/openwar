#pragma once

#include <Urho3D/Math/Ray.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Scene/Serializable.h>

using namespace Urho3D;

class Coordinator;

#define FORMATION_CREATETYPE(FORMATION, TYPE, CONTEXT) switch (TYPE) { \
case FORMATION_GRID:                                          \
	FORMATION = new GridFormation(CONTEXT);                  \
	break;                                                    \
case FORMATION_HEX:                                           \
	FORMATION = new HexFormation(CONTEXT);                   \
    break;                                                    \
default:                                                      \
	FORMATION = new Formation(CONTEXT);                      \
	break;                                                    \
}

enum FormationType : int {
	FORMATION_NONE,
	FORMATION_GRID,
	FORMATION_HEX
};

const unsigned MAX_FORMATION_SPOT_CONNECTIONS = 3;

struct FormationSpot {
	unsigned id;

	// Todo - add connection priority strength? So soldiers in combat have higher priority to "pull" reinforcements in,
	// or the sides of the formation have more strength than the random people standing in the center of the formation
	unsigned connections[MAX_FORMATION_SPOT_CONNECTIONS];

	IntVector2 fileAndRank;

	Vector3 position;
	Vector3 direction;
};

class Formation : public Serializable {
	URHO3D_OBJECT(Formation, Serializable);

public:
	/// Construct.
	Formation(Context* context);
	
	static void RegisterObject(Context* context);

	void AutoGenConnections(float radius);

	virtual void Set(Vector3 startPoint, Vector3 endPoint, unsigned count);

	void AddSpot(FormationSpot spot) { dirty_ = true; formationSpots_.Push(spot); }
	void SetSpot(unsigned id, FormationSpot spot) { dirty_ = true;  formationSpots_[id] = spot; }

	Vector3 GetCentre();
	Vector3 GetFront();
	Vector3 GetStartPoint() { return startPoint_; }
	Vector3 GetEndPoint() { return endPoint_; }

	FormationType GetFormationType() { return type_; }

	FormationSpot GetSpot(unsigned id) { return formationSpots_[id]; }
	FormationSpot GetSpot(IntVector2 pos);
	const PODVector<FormationSpot>& GetSpots();

	PODVector<FormationSpot> GetFile(int file);
	PODVector<FormationSpot> GetRank(int rank);

	unsigned GetSpotCount() { return formationSpots_.Size(); }
	unsigned GetFileCount() { return fileCount_; }
	unsigned GetRankCount() { return rankCount_; }

	virtual SharedPtr<Formation> CloneDetails() const;

	// None formation
	static const Formation NONE;
protected:
	void RecalculateCentre();

	VariantVector GetFormationSpotsAttr() const;
	void SetFormationSpotsAttr(const VariantVector& value);

	FormationType type_ = FORMATION_NONE;

	unsigned fileCount_ = 0;
	unsigned rankCount_ = 0;

	PODVector<FormationSpot> formationSpots_;
	
	Vector3 startPoint_;
	Vector3 endPoint_;

private:
	Vector3 centre_;
	bool dirty_ = true;
};

class GridFormation : public Formation {
	URHO3D_OBJECT(GridFormation, Formation);

public:
	/// Construct.
	GridFormation(Context* context);

	static void RegisterObject(Context* context);

	virtual void Set(Vector3 startPoint, Vector3 endPoint, unsigned count);

	virtual SharedPtr<Formation> CloneDetails() const;

	// Hastati
	//float rowSpacing_ = 1.37f;
	//float columnSpacing_ = 1.37f;

	// Hoplite
	//float rowSpacing_ = 0.9f;
	//float columnSpacing_ = 0.65f;

	// Phalanx
	float rowSpacing_ = 0.9f;
	float columnSpacing_ = 0.9f;
};

class HexFormation : public Formation {
	URHO3D_OBJECT(HexFormation, Formation);

public:
	/// Construct.
	HexFormation(Context* context);

	static void RegisterObject(Context* context);

	virtual void Set(Vector3 startPoint, Vector3 endPoint, unsigned count);

	virtual SharedPtr<Formation> CloneDetails() const;

	// Hastati
	float rowSpacing_ = 1.37f;
	float columnSpacing_ = 1.37f;

	// Hoplite
	//float rowSpacing_ = 0.9f;
	//float columnSpacing_ = 0.65f;

	// Phalanx
	//float rowSpacing_ = 0.9f;
	//float columnSpacing_ = 0.9f;
};