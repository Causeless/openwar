#pragma once

#include <Urho3D/Math/Vector2.h>
#include <Urho3D/Math/Vector3.h>

using namespace Urho3D;

class VecUtils {
public:
	static Vector2 toVec2(Vector3 vec)
	{
		return Vector2(vec.x_, vec.z_);
	};

	static Vector3 toVec3(Vector2 vec)
	{
		return Vector3(vec.x_, 0.0f, vec.y_);
	};

	static Vector3 flatten(Vector3 vec)
	{
		vec.y_ = 0.0f;
		return vec;
	}
};