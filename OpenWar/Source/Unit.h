#pragma once

#include <Urho3D/Scene/Component.h>
#include "Coordinator/Coordinator.h"

#include "SoldierModel.h"
#include "UnitStats.h"

using namespace Urho3D;

class Coordinator;

// A unit
class Unit : public Component {
	URHO3D_OBJECT(Unit, Component);

public:
	Unit(Context* context);
	~Unit();

	static void RegisterObject(Context* context);

	void SetDirty() { dirty_ = true; }
	void SetSoldierCount(unsigned soldierCount) { soldierCount_ = soldierCount; }
	void SetTeam(int teamId) { teamId_ = teamId; }

	void Spawn(UnitStats stats, SharedPtr<Formation> formation);

	void Attack(Node* target);

	PODVector<Node*> GetSoldiers();
	Node* GetSoldier(int i) { return soldiers_[i]; }

	Node* GetClosestSoldierToPoint(Vector3 point);

	Vector3 GetPosition() const;
	Vector3 GetVelocity() const;

	Coordinator* GetCoordinator() const;

	void SetSoldierModel(SoldierModel model) { soldierModel_ = model; }
	void SetSoldierAnimSet(SoldierAnimSet animSet) { soldierAnimSet_ = animSet; }

	UnitStats& GetUnitStats();

	// Hurts the unit, and returns whether or not a soldier should be killed
	bool Hurt();

protected:
	/// Handle node being assigned.
	virtual void OnNodeSet(Node* node);

private:
	void UpdateSim(StringHash eventType, VariantMap& eventData);
	void UpdatePosition();
	void SpawnSoldier(Vector3 pos, Vector3 dir);

	PODVector<Node*> soldiers_;
	
	Vector3 averagePosition_;
	Vector3 averageVelocity_;

	Node* attackTarget_ = nullptr;

	SoldierModel soldierModel_;
	SoldierAnimSet soldierAnimSet_;

	UnitStats unitStats_;

	// How many HP this unit has. When an attack is successfully resolved, we lower HP.
	int healthPoints_;
	// How many HP we must lose until we determine that a attack resolution kills our soldier
	int healthPointsPerSoldier_;

	unsigned soldierCount_ = 0;
	int teamId_ = 0;
	bool dirty_ = false;
};