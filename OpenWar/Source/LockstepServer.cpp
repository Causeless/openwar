#include "LockstepServer.h"

#include <Urho3D/Engine/Application.h>
#include <Urho3D/Engine/Console.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Engine/EngineDefs.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/SceneEvents.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Resource/XMLFile.h>
#include <Urho3D/IO/Log.h>
#include <Urho3D/IO/FileSystem.h>

#include <Urho3D/Network/Connection.h>
#include <Urho3D/Network/NetworkEvents.h>
#include <Urho3D/Network/Network.h>

#include "Command.h"

LockstepServer::LockstepServer(Context* context) :
    LogicComponent(context)
{
}

void LockstepServer::RegisterObject(Context* context)
{
	context->RegisterFactory<LockstepServer>("LOCKSTEP");
}

struct PhysicsFrameInfo {
	int physicsTick = 0;
	int checksum = 0;
};

struct Client {
	Connection* connection = 0;

	// The clients most recent frame info
	PhysicsFrameInfo frameInfo;

	// The clients frame info on the last TICK_BATCH_SIZE*2 divisible frame; this is used to compare checksums with other clients
	PhysicsFrameInfo syncedFrameInfo;
};

void LockstepServer::Start()
{
	SubscribeToEvent(E_CLIENTCONNECTED, URHO3D_HANDLER(LockstepServer, HandleClientConnected));
	SubscribeToEvent(E_CLIENTDISCONNECTED, URHO3D_HANDLER(LockstepServer, HandleClientDisconnected));

	SubscribeToEvent(E_NETWORKMESSAGE, URHO3D_HANDLER(LockstepServer, HandleNetworkMessage));

	SubscribeToEvent(E_CLIENTSTEP, URHO3D_HANDLER(LockstepServer, HandleClientStep));
	GetSubsystem<Network>()->RegisterRemoteEvent(E_CLIENTSTEP);
}

void LockstepServer::StartServer() {
	Network* network = GetSubsystem<Network>();
	network->SetUpdateFps(30);

	network->StartServer(SERVER_PORT);

	simToPhysicsTick_ = TICK_BATCH_SIZE;
}

void LockstepServer::StopServer()
{
	Network* network = GetSubsystem<Network>();
	network->StopServer();

	simToPhysicsTick_ = TICK_BATCH_SIZE;

#ifdef DEBUG
	assert(false);
#endif // DEBUG
}

void LockstepServer::HandleNetworkMessage(StringHash eventType, VariantMap& eventData)
{
	using namespace NetworkMessage;

	Network* network = GetSubsystem<Network>();
	// We set the tick for the command to be applied then broadcast the message back out to all clients for them to deserialize and handle
	if (eventData[P_MESSAGEID].GetInt() == M_SERVERCOMMAND) {
		VectorBuffer buf = eventData[P_DATA].GetBuffer();
		
		Command* command;
		CommandType type = (CommandType)buf.ReadInt();
		COMMAND_CREATETYPE(command, type);

		command->Load(buf);

		command->SetApplicationTick(simToPhysicsTick_ + TICK_BATCH_SIZE);

		VectorBuffer newBuf = VectorBuffer();
		newBuf.WriteInt(type);
		command->Save(newBuf);
		delete command;

		network->BroadcastMessage(M_CLIENTCOMMAND, true, true, newBuf);
	}
}

void LockstepServer::HandleClientStep(StringHash eventType, VariantMap& eventData)
{
	using namespace RemoteEventData;
	Connection* remoteSender = static_cast<Connection*>(eventData[P_CONNECTION].GetPtr());

	PhysicsFrameInfo info;
	info.physicsTick = eventData[P_TICK].GetInt();
	info.checksum = eventData[P_CHECKSUM].GetInt();

	clients_[remoteSender].frameInfo = info;

	if ((info.physicsTick % (TICK_BATCH_SIZE*2)) == 0) {
		clients_[remoteSender].syncedFrameInfo = info;
	}

	UpdateTick();
}

void LockstepServer::UpdateTick() {
	auto clientsVector = clients_.Values();

	for (unsigned i = 0; i < clientsVector.Size(); i++) {
		if (clientsVector[i].frameInfo.physicsTick + TICK_BATCH_SIZE - 1 < simToPhysicsTick_) {
			return;
		}
	}

	// If we have gotten here, all clients must be near caught up with eachother, so we can increment it and broadcast update

	bool checksumMismatch = ChecksumMismatch(clientsVector);
	
	if (checksumMismatch)
		StopServer();

	simToPhysicsTick_ += TICK_BATCH_SIZE;

	Network* network = GetSubsystem<Network>();

	VariantMap remoteEventData;
	remoteEventData[P_TICK] = simToPhysicsTick_;
	network->BroadcastRemoteEvent(E_STEP, true, remoteEventData);
}

bool LockstepServer::ChecksumMismatch(Vector<Client> &clientsVector) {
	bool checksumMismatch = false;
	
	if (clientsVector.Size() > 0) {
		int checksum = clientsVector[0].syncedFrameInfo.checksum;
		int tick = clientsVector[0].syncedFrameInfo.physicsTick;

		for (unsigned i = 1; i < clientsVector.Size(); i++) {
			if (clientsVector[i].syncedFrameInfo.checksum != checksum && 
				clientsVector[i].syncedFrameInfo.physicsTick == tick) {

				checksumMismatch = true;
				break;
			}
		}

		if (checksumMismatch) {
			// Checksum mismatch! print checksums
			Log::Write(1, "Checksum Mismatch on tick " + String(clientsVector[0].syncedFrameInfo.physicsTick) + "! Printing checksums:");
			for (unsigned i = 0; i < clientsVector.Size(); i++) {
				Connection* conn = clientsVector[i].connection;
				String clientAddress = conn->GetAddress() + ":" + String(conn->GetPort());
				Log::Write(1, 
					"\tClient: " + clientAddress + 
					", Checksum: " + String(clientsVector[i].syncedFrameInfo.checksum) +
					", Tick: " + String(clientsVector[i].syncedFrameInfo.physicsTick)
				);
			}
		}
	}

	return checksumMismatch;
}

void LockstepServer::HandleClientConnected(StringHash eventType, VariantMap& eventData)
{
	using namespace ClientConnected;

	// When a client connects, assign to scene to begin scene replication
	Connection* newConnection = static_cast<Connection*>(eventData[P_CONNECTION].GetPtr());
	//newConnection->SetScene(scene_);

	clients_[newConnection] = Client();
	clients_[newConnection].connection = newConnection;

	VariantMap remoteEventData;
	remoteEventData[P_TICK] = simToPhysicsTick_;
	newConnection->SendRemoteEvent(E_STEP, true, remoteEventData);
}

void LockstepServer::HandleClientDisconnected(StringHash eventType, VariantMap& eventData)
{
	using namespace ClientDisconnected;

	// When a client connects, assign to scene to begin scene replication
	Connection* connection = static_cast<Connection*>(eventData[P_CONNECTION].GetPtr());
	//newConnection->SetScene(scene_);

	if (clients_.Contains(connection)) {
		clients_.Erase(connection);
	}
}