#include "Unit.h"
#include "UnitSelector.h"
#include "UnitController.h"
#include "VecUtils.h"
#include "Command.h"

#include <Urho3D/Core/Context.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/DebugRenderer.h>
#include <Urho3D/Graphics/Drawable.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/UIEvents.h>
#include <Urho3D/Navigation/NavigationMesh.h>
#include <Urho3D/UI/BorderImage.h>
#include <Urho3D/Network/Network.h>

#include <Urho3D/IO/Log.h>

UnitSelector::UnitSelector(Context* context) : Component(context) {

}

void UnitSelector::RegisterObject(Context* context)
{
	context->RegisterFactory<UnitSelector>("UNIT_SELECTOR");
}

void UnitSelector::OnNodeSet(Node* node)
{
	if (!node) return;

	camera_ = node_->GetComponent<Camera>();
	ui_ = GetSubsystem<UI>();
	input_ = GetSubsystem<Input>();
	graphics_ = GetSubsystem<Graphics>();

	selectionBoxImage_ = new BorderImage(context_);
	selectionBoxImage_->SetOpacity(0.5);
	selectionBoxImage_->SetColor(Color::BLACK);

	ui_->GetRoot()->AddChild(selectionBoxImage_);

	SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(UnitSelector, HandleUpdate));

	// Subscribe to mouse events
	SubscribeToEvent(E_MOUSEBUTTONDOWN, URHO3D_HANDLER(UnitSelector, HandleMouseButtonDown));
	SubscribeToEvent(E_MOUSEBUTTONUP, URHO3D_HANDLER(UnitSelector, HandleMouseButtonUp));
}

bool UnitSelector::Raycast(float maxDistance, Vector3& hitPos, Drawable*& hitDrawable, unsigned int viewMask)
{
	hitDrawable = nullptr;

	IntVector2 pos = ui_->GetCursorPosition();
	// Check the cursor is visible and there is no UI element in front of the cursor
	if (!ui_->GetCursor()->IsVisible() || ui_->GetElementAt(pos, true))
		return false;

	Ray cameraRay = camera_->GetScreenRay((float)pos.x_ / graphics_->GetWidth(), (float)pos.y_ / graphics_->GetHeight());
	// Pick only geometry objects, not eg. zones or lights, only get the first (closest) hit
	auto results = PODVector<RayQueryResult>();
	RayOctreeQuery query(results, cameraRay, RAY_TRIANGLE, maxDistance, DRAWABLE_GEOMETRY, viewMask);
	GetScene()->GetComponent<Octree>()->RaycastSingle(query);
	if (results.Size())
	{
		RayQueryResult& result = results[0];
		hitPos = result.position_;
		hitDrawable = result.drawable_;
		return true;
	}

	return false;
}

bool UnitSelector::HasSelectedUnit() {
	return !selectedUnits_.Empty();
}

HashSet<Unit*> UnitSelector::GetSelectedUnits() {
	return selectedUnits_;
}

void UnitSelector::SelectUnit(Unit* unit, bool overwrite) {
	if (overwrite)
		selectedUnits_.Clear();

	assert(unit);
	selectedUnits_.Insert(unit);
}

void UnitSelector::ClearSelection() {
	selectedUnits_.Clear();
}

void UnitSelector::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
	IntVector2 cursorPositionScreen = ui_->GetCursorPosition();

	UpdateSelectionBox(cursorPositionScreen);

	if (input_->GetMouseButtonDown(MOUSEB_RIGHT) && HasSelectedUnit()) {
		Vector3 curPos;
		Drawable* hitDrawable;

		Raycast(1000.0f, curPos, hitDrawable, 0x00000001); // Set current position

		if ((clickStartWorld_ - curPos).LengthSquared() > 6.0f*6.0f*selectedUnits_.Size()*selectedUnits_.Size()) {
			DebugRenderer* debug = GetScene()->GetComponent<DebugRenderer>();

			auto unitsFormations = GetUnitFormationsForLine(clickStartWorld_, curPos);

			for (auto it = unitsFormations.Begin(); it != unitsFormations.End(); ++it) {
				SharedPtr<Formation> formation = it->second_;

				for (unsigned i = 0; i < formation->GetSpotCount(); i++) {
					NavigationMesh* navmesh = GetScene()->GetComponent<NavigationMesh>();
					Vector3 targetPosition = navmesh->FindNearestPoint(formation->GetSpot(i).position, Vector3(0.0f, 1000.0f, 0.0f));

					Sphere sphere = Sphere(targetPosition, 0.25f);
					debug->AddSphere(sphere, Color(255, 0, 0), true);
				}
			}
		}
	}
}

PODVector<Unit*> GetUnitsSortedAlongDirection(const HashSet<Unit*>& units, Vector3 direction) {
	PODVector<Unit*> sortedUnits(0);
	for (auto it = units.Begin(); it != units.End(); ++it) {
		Unit* unit = it.operator*();
		sortedUnits.Push(unit);
	}

	Sort(sortedUnits.Begin(), sortedUnits.End(), 
		[direction](const Unit* lhs, const Unit* rhs) {
			return lhs->GetPosition().ProjectOntoAxis(direction) < rhs->GetPosition().ProjectOntoAxis(direction);
		}
	);

	return sortedUnits;
}

HashMap<Unit*, SharedPtr<Formation>> UnitSelector::GetUnitFormationsForLine(Vector3 startPoint, Vector3 endPoint) {
	auto returnMap = HashMap<Unit*, SharedPtr<Formation>>();
	float spaceBetweenUnits = GridFormation(context_).columnSpacing_*0.5f;

	Vector3 directionAlongFront = (endPoint - startPoint).Normalized();
	float length = (endPoint - startPoint).Length();
	float lengthPerUnit = (length - spaceBetweenUnits* selectedUnits_.Size()) / selectedUnits_.Size();

	Vector3 currentStartPoint = startPoint;

	// Sort units by their distance along the line, so that they are done in the right order
	auto sortedUnits = GetUnitsSortedAlongDirection(selectedUnits_, directionAlongFront);

	for (auto it = sortedUnits.Begin(); it != sortedUnits.End(); ++it) {
		Unit* unit = it.operator*();

		Vector3 currentEndPoint = currentStartPoint + directionAlongFront *lengthPerUnit;

		SharedPtr<Formation> formation = unit->GetCoordinator()->GetFormation()->CloneDetails();
		formation->Set(currentStartPoint, currentEndPoint, unit->GetSoldiers().Size());

		returnMap[unit] = formation;

		currentStartPoint = currentEndPoint + directionAlongFront*spaceBetweenUnits;
	}

	return returnMap;
}

void UnitSelector::UpdateSelectionBox(IntVector2 cursorPositionScreen)
{
	// We need to choose whether to use start pos or current pos as box position, as it will not render negative sizes
	int minX = Min(clickStartScreen_.x_, cursorPositionScreen.x_);
	int minY = Min(clickStartScreen_.y_, cursorPositionScreen.y_);
	int maxX = Max(clickStartScreen_.x_, cursorPositionScreen.x_);
	int maxY = Max(clickStartScreen_.y_, cursorPositionScreen.y_);

	IntVector2 topLeftPixel = IntVector2(minX, minY);
	IntVector2 bottomRightPixel = IntVector2(maxX, maxY);

	// For logic purposes (actual unit selection) we need to transform from pixels into camera space 0 to 1
	Vector2 screenSize = Vector2((float)graphics_->GetWidth(), (float)graphics_->GetHeight());

	Vector2 topLeftCamera = (Vector2)topLeftPixel / screenSize;
	Vector2 bottomRightCamera = (Vector2)bottomRightPixel / screenSize;

	selectionBox_ = Rect(topLeftCamera, bottomRightCamera);

	// We're only selecting if the box is big enough
	isSelecting_ = input_->GetMouseButtonDown(MOUSEB_LEFT) && selectionBox_.Size().LengthSquared() > 0.02f*0.02f;

	if (isSelecting_) {
		selectionBoxImage_->SetPosition(topLeftPixel);
		selectionBoxImage_->SetSize(bottomRightPixel - topLeftPixel);
	} else {
		selectionBoxImage_->SetSize(IntVector2::ZERO);
	}
}

HashSet<Unit*> UnitSelector::GetUnitsInSelectionBox()
{
	HashSet<Unit*> unitsInSelectionBox = HashSet<Unit*>();

	PODVector<Node*> unitNodes = PODVector<Node*>();
	GetScene()->GetChildrenWithComponent<Unit>(unitNodes, true);

	for (unsigned i = 0; i < unitNodes.Size(); i++) {
		Node* unitNode = unitNodes[i];
		Unit* unit = unitNode->GetComponent<Unit>();

		for (auto soldier : unit->GetSoldiers()) {
			// We can safely get the interpolated position instead of real agent position here,
			// as the UI selection isn't synced (only actual commands are)
			Vector2 screenPos = camera_->WorldToScreenPoint(soldier->GetPosition());
			if (selectionBox_.IsInside(screenPos)) {
				unitsInSelectionBox.Insert(unit);
				break;
			}
		}
	}

	return unitsInSelectionBox;
}

void UnitSelector::HandleMouseButtonDown(StringHash eventType, VariantMap& eventData)
{
	UI* ui = GetSubsystem<UI>();
	Input* input = GetSubsystem<Input>();

	clickStartScreen_ = ui->GetCursorPosition();

	Drawable* hitDrawable;
	Raycast(1000.0f, clickStartWorld_, hitDrawable, 0x00000001); // Set startPos

	int mouseButton = eventData[MouseButtonDown::P_BUTTON].GetInt();
	// 1 is left, 2 is middle, 3 is right
	if (mouseButton == MOUSEB_LEFT) {
		Vector3 pos;
		Drawable* hitDrawable;

		Raycast(1000.0f, pos, hitDrawable);

		if (hitDrawable) {
			Node* parentNode = hitDrawable->GetNode()->GetParent();
			if (parentNode) {
				Unit* hitUnit = parentNode->GetComponent<Unit>();
				if (hitUnit) {
					SelectUnit(hitUnit);
				}
			}
		}
	}
}

void UnitSelector::HandleMouseButtonUp(StringHash eventType, VariantMap& eventData)
{
	UnitController* unitController = node_->GetComponent<UnitController>();

	int mouseButton = eventData[MouseButtonDown::P_BUTTON].GetInt();

	if (mouseButton == MOUSEB_LEFT && isSelecting_) {
		selectedUnits_ = GetUnitsInSelectionBox();
		isSelecting_ = false;
	}

	// 1 is left, 2 is middle, 3 is right
	if (mouseButton == MOUSEB_RIGHT && HasSelectedUnit()) {
		Vector3 endPos;
		Drawable* hitDrawable;

		Raycast(1000.0f, endPos, hitDrawable, 0x00000001); // Set startPos

		if ((clickStartWorld_ - endPos).LengthSquared() > 6.0f*6.0f*selectedUnits_.Size()*selectedUnits_.Size()) 
		{
			unitController->SetFormation(clickStartWorld_, endPos);
		} 
		else if (selectedUnits_.Size() == 2) 
		{
			AttackCommand command1(context_);
			command1.Set(selectedUnits_.Front(), selectedUnits_.Back());

			AttackCommand command2(context_);
			command2.Set(selectedUnits_.Back(), selectedUnits_.Front());

			Connection* connection = GetSubsystem<Network>()->GetServerConnection();
			if (connection) {
				VectorBuffer buf1 = VectorBuffer();
				buf1.WriteInt(COMMAND_ATTACK);
				command1.Save(buf1);

				VectorBuffer buf2 = VectorBuffer();
				buf2.WriteInt(COMMAND_ATTACK);
				command2.Save(buf2);

				connection->SendMessage(M_SERVERCOMMAND, true, true, buf1);
				connection->SendMessage(M_SERVERCOMMAND, true, true, buf2);
			}
		}
	}
}