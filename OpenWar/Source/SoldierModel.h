#pragma once

#include <Urho3D/Core/StringUtils.h>

struct SoldierModel {
	 Urho3D::String model;
	 Urho3D::String material;
};

struct SoldierAnimSet {
	Urho3D::String attack;
	Urho3D::String defend;
	Urho3D::String idle;
	Urho3D::String walk;
	Urho3D::String run;
};