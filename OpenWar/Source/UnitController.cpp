#include "UnitController.h"
#include "UnitSelector.h"
#include "Formation.h"
#include "VecUtils.h"
#include "Command.h"

#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Navigation/NavigationMesh.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Network/Network.h>
#include <Urho3D/Network/NetworkEvents.h>

#include <Urho3D/IO/Log.h>

UnitController::UnitController(Context* context) : Component(context) {

}

void UnitController::RegisterObject(Context* context)
{
	context->RegisterFactory<UnitController>("UNIT_CONTROLLER");
}

void UnitController::OnNodeSet(Node* node) 
{

}

void UnitController::SetFormation(Vector3 startPoint, Vector3 endPoint)
{
	UnitSelector* selector = node_->GetComponent<UnitSelector>();
	if (!selector->HasSelectedUnit()) return;

	auto units = selector->GetUnitFormationsForLine(startPoint, endPoint);

	for (auto keyVal = units.Begin(); keyVal != units.End(); keyVal++) {
		Unit* unit = keyVal->first_;
		SharedPtr<Formation> formation = keyVal->second_;

		MovementCommand command(context_);
		command.Set(unit, *formation);

		VectorBuffer buf = VectorBuffer();
		buf.WriteInt(COMMAND_MOVEMENT);
		command.Save(buf);

		Connection* connection = GetSubsystem<Network>()->GetServerConnection();
		if (connection) {
			connection->SendMessage(M_SERVERCOMMAND, true, true, buf);
		}
	}
}