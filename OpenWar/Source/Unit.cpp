#include "Unit.h"
#include "Formation.h"
#include "VecUtils.h"
#include "Coordinator/Coordinator.h"
#include "Soldier.h"
#include "UnitStats.h"

#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Graphics/AnimatedModel.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Graphics/AnimationController.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Navigation/CrowdAgent.h>
#include <Urho3D/Navigation/NavigationMesh.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Physics/PhysicsEvents.h>

#include <Urho3D/Graphics/DebugRenderer.h>

#include <Urho3D/Graphics/ParticleEmitter.h>
#include <Urho3D/Graphics/ParticleEffect.h>

#include <Urho3D/IO/Log.h>

Unit::Unit(Context* context) : Component(context) {
	
}

Unit::~Unit() {

}

void Unit::RegisterObject(Context* context)
{
	context->RegisterFactory<Unit>("UNIT");
}

void Unit::OnNodeSet(Node* node)
{
	if (node) {
		SubscribeToEvent(E_PHYSICSPRESTEP, URHO3D_HANDLER(Unit, UpdateSim));
	}
}

// Todo: create a separate unitspawner.
void Unit::Spawn(UnitStats unitStats, SharedPtr<Formation> formation) {
	unitStats_ = unitStats;
	
	healthPoints_ = soldierCount_ * unitStats.healthPointsPerSoldier_;
	healthPointsPerSoldier_ = unitStats.healthPointsPerSoldier_;

	soldiers_ = PODVector<Node*>();

	auto crowdManager = node_->GetParentComponent<CrowdManager>();
	auto navMesh = node_->GetParentComponent<NavigationMesh>();

	Vector3 position = node_->GetWorldPosition();
	Vector3 direction = node_->GetWorldDirection();

	Vector3 startPoint = position + (direction.Orthogonalize(Vector3::UP) * 10.0f);
	Vector3 endPoint = position - (direction.Orthogonalize(Vector3::UP) * 10.0f);

	// Now we need to set our transform to avoid transforming children soldiers
	// TODO find a better way, Unit class probably shouldn't be a node?
	node_->SetTransform(Matrix3x4::IDENTITY);

	formation->Set(startPoint, endPoint, soldierCount_);
	for (unsigned i = 0; i < soldierCount_; i++) {
		FormationSpot spot = formation->GetSpot(i);

		Vector3 spawnPoint = navMesh->FindNearestPoint(spot.position, Vector3(1.0f, 1000.0f, 1.0f));
		SpawnSoldier(spawnPoint, spot.direction);
	}

	// Update our list of child soldiers
	node_->GetChildrenWithComponent<CrowdAgent>(soldiers_, false);

	UpdatePosition();
	averageVelocity_ = Vector3::ZERO;

	//TODO; Fix; Make this work generically with just Coordinator somehow and not specific type
	node_->GetComponent<LocalFormationCoordinator>()->SetFormation(formation);
	
}

void Unit::Attack(Node* target)
{
	attackTarget_ = target;
}

Node* Unit::GetClosestSoldierToPoint(Vector3 point)
{
	Node* closest = nullptr;
	float closestDistanceSqr = FLT_MAX;

	for (auto soldier : GetSoldiers()) {
		// TODO. Fix this, I'm lazy and putting this here, but really I need to analyze update order better to understand this.
		if (!soldier->GetComponent<CrowdAgent>()) {
			continue;
		}

		// Get synced position, not node
		float distanceSqr = (point - soldier->GetComponent<CrowdAgent>()->GetPosition()).LengthSquared();

		if (distanceSqr < closestDistanceSqr) {
			closest = soldier;
			closestDistanceSqr = distanceSqr;
		}
	}

	return closest;
}

void Unit::UpdateSim(StringHash eventType, VariantMap& eventData)
{
	using namespace PhysicsPreStep;

	// Take the frame time step, which is stored as a float
	float timeStep = eventData[P_TIMESTEP].GetFloat();

	//TODO; Fix; Make this work generically with just Coordinator somehow and not specific type
	Coordinator* coordinator = node_->GetComponent<LocalFormationCoordinator>();
	Vector3 oldPos = averagePosition_;

	if (dirty_) {
		// Update our list of child soldiers
		node_->GetChildrenWithComponent<CrowdAgent>(soldiers_, false);

		Formation* formation = coordinator->GetFormation();
		formation->Set(formation->GetStartPoint(), formation->GetEndPoint(), soldiers_.Size());
		coordinator->SetFormation(formation);

		dirty_ = false;
	}

	// Update our average position
	UpdatePosition();

	// Update our average velocity
	averageVelocity_ = averagePosition_ - oldPos;

	coordinator->UpdateSoldierTargets(timeStep);

	if (attackTarget_) {
		// Update our formation to match our current position, and face towards enemy
		Formation* formation = coordinator->GetFormation();
		Vector3 relativeStartPos = formation->GetStartPoint() - formation->GetCentre();
		Vector3 relativeEndPos = formation->GetEndPoint() - formation->GetCentre();

		Vector3 forwards = (formation->GetFront() - formation->GetCentre()).Normalized();
		Vector3 towardsEnemy = (attackTarget_->GetComponent<Unit>()->GetPosition() - GetPosition()).Normalized();

		// Need to zero out z axis so the set line doesn't rotate up into the sky.
		forwards.y_ = towardsEnemy.y_ = 0.0f;

		float distanceSqr = (attackTarget_->GetComponent<Unit>()->GetPosition() - GetPosition()).LengthSquared();

		// Only try to rotate if we're not super closer, so we don't have funky things where we suddenly flip around.
		Quaternion rotate = distanceSqr > 3.0f*3.0f ? Quaternion(forwards, towardsEnemy) : Quaternion::IDENTITY;

		formation->Set(averagePosition_ + (rotate * relativeStartPos), averagePosition_ + (rotate * relativeEndPos), soldiers_.Size());
		coordinator->SetFormationNoReassign(formation);

		auto frontLine = coordinator->GetFormation()->GetRank(0);
		//frontLine += coordinator->GetFormation()->GetRank(1);

		//auto frontLine = coordinator->GetFormation()->GetSpots();

		for (auto spot : frontLine) {
			Node* attacker = coordinator->GetAssignment()->GetSoldierForSpot(spot.id);
			Vector3 attackerPos = attacker->GetComponent<CrowdAgent>()->GetPosition();

			Node* defender = attackTarget_->GetComponent<Unit>()->GetClosestSoldierToPoint(attackerPos);
			attacker->GetComponent<Soldier>()->SetToAttack(defender);
		}
	}
}

void Unit::UpdatePosition() {
	Vector3 oldAvg = averagePosition_;
	// Update our nodes position to the average of the unit
	averagePosition_ = Vector3::ZERO;
	auto soldiers = PODVector<Node*>();
	node_->GetChildrenWithComponent<CrowdAgent>(soldiers, false);
	for (auto soldier : soldiers) {
		averagePosition_ += soldier->GetComponent<CrowdAgent>()->GetPosition();
	}

	averagePosition_ /= (float)soldiers.Size();
}

Vector3 Unit::GetPosition() const {
	return averagePosition_;
}

Vector3 Unit::GetVelocity() const {
	return averageVelocity_;
}

Coordinator* Unit::GetCoordinator() const
{
	//TODO; Fix; Make this work generically with just Coordinator somehow and not specific type
	return node_->GetComponent<LocalFormationCoordinator>();
}

UnitStats& Unit::GetUnitStats() {
	return unitStats_;
}

void Unit::SpawnSoldier(Vector3 pos, Vector3 dir)
{
	// We create soldiers as local nodes and not network-replicated as they are simulated on each client independently
	Node* soldierNode(node_->CreateChild("Soldier", LOCAL));
	Soldier* soldier = soldierNode->CreateComponent<Soldier>(LOCAL);

	// Need to scale for RTW models
	soldierNode->SetScale(Vector3(-1.0f, 1.0f, -1.0f));

	soldierNode->SetPosition(pos);
	soldierNode->SetDirection(dir);

	soldier->Spawn(soldierModel_, &soldierAnimSet_);
}

PODVector<Node*> Unit::GetSoldiers()
{
	return soldiers_;
}

bool Unit::Hurt()
{
	healthPoints_--;

	if (healthPoints_ <= 0) {
		node_->SetEnabled(false);
	}

	return healthPoints_ % healthPointsPerSoldier_ == 0;
}