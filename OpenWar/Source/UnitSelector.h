#pragma once

#include "Unit.h"

using namespace Urho3D;

namespace Urho3D
{
	class Drawable;
	class Node;
	class Scene;
	class BorderImage;

	class Camera;
	class UI;
	class Input;
	class Graphics;
}

// UnitSelector is for a user to select units, i.e by click-drag frustum
class UnitSelector : public Component {
	URHO3D_OBJECT(UnitSelector, Component);

public:
	UnitSelector(Context* context);
	static void RegisterObject(Context* context);

	bool HasSelectedUnit();
	void SelectUnit(Unit* unit, bool overwrite = true);
	void ClearSelection();

	HashSet<Unit*> GetSelectedUnits();
	HashMap<Unit*, SharedPtr<Formation>> GetUnitFormationsForLine(Vector3 startPoint, Vector3 endPoint);

protected:
	/// Handle node being assigned.
	virtual void OnNodeSet(Node* node);

	void HandleUpdate(StringHash eventType, VariantMap& eventData);
	void HandleMouseButtonDown(StringHash eventType, VariantMap& eventData);
	void HandleMouseButtonUp(StringHash eventType, VariantMap& eventData);

private:
	HashSet<Unit*> GetUnitsInSelectionBox();
	void UpdateSelectionBox(IntVector2 cursorPositionScreen);

	/// Utility function to raycast to the cursor position. Return true if hit.
	bool Raycast(float maxDistance, Vector3& hitPos, Drawable*& hitDrawable, unsigned int viewMask = 0xFFFFFFFF);

	/// Start click
	Vector3 clickStartWorld_;
	IntVector2 clickStartScreen_;

	bool isSelecting_ = false; // If the user is currently dragging a selection box out

	Camera* camera_;
	UI* ui_;
	Input* input_;
	Graphics* graphics_;

	Rect selectionBox_;
	SharedPtr<BorderImage> selectionBoxImage_;

	HashSet<Unit*> selectedUnits_;
};