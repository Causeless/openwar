#include "Coordinator.h"
#include "../VecUtils.h"
#include "../Unit.h"
#include "../Soldier.h"
#include "../SpotAssignment.h"
#include "../Formation.h"
#include "../SyncedRandom.h"

#include <Urho3D/Core/Context.h>
#include <Urho3D/Navigation/NavigationMesh.h>
#include <Urho3D/Navigation/CrowdAgent.h>
#include <Urho3D/Scene/Node.h>

FormationCoordinator::FormationCoordinator(Context* context) : Coordinator(context)
{

}

void FormationCoordinator::RegisterObject(Context* context)
{
	context->RegisterFactory<FormationCoordinator>("COORDINATOR");
}

void FormationCoordinator::SetFormation(Formation* formation) {
	unit_ = node_->GetComponent<Unit>();
	worldSpaceFormation_ = SharedPtr<Formation>(formation);
	CreateUnitSpaceFormation();
	UpdateFormationRotation();

	assignment_ = HungarianAlgorithmSpotAssignment(*unit_, *GetRotatedFormation(unitSpaceFormation_));
	//ticksUntilAssignRefresh_ = (SyncedRandom::Rand() % 50);
}

void FormationCoordinator::SetFormationNoReassign(Formation* formation) {
	unit_ = node_->GetComponent<Unit>();
	worldSpaceFormation_ = SharedPtr<Formation>(formation);
	CreateUnitSpaceFormation();
	UpdateFormationRotation();
}

void FormationCoordinator::UpdateSoldierTargets(float dt) {
	if (!worldSpaceFormation_ || !unitSpaceFormation_) {
		return;
	}

	UpdateFormationRotation();
	auto rotatedFormation = GetRotatedFormation(unitSpaceFormation_);

	if (ticksUntilAssignRefresh_ <= 0) {
		// Todo; do something else that won't lag
		//assignment_ = HungarianAlgorithmSpotAssignment(*node_->GetComponent<Unit>(), *worldSpaceFormation_);
		//ticksUntilAssignRefresh_ = (SyncedRandom::Rand() % 50);
	}

	ticksUntilAssignRefresh_--;

	auto navMesh = node_->GetParentComponent<NavigationMesh>();

	Vector3 unitDesiredMovement = worldSpaceFormation_->GetCentre() - node_->GetComponent<Unit>()->GetPosition();
	Vector3 unitDesiredMovementDirection = unitDesiredMovement.Normalized();

	for (auto soldierNode : unit_->GetSoldiers()) {
		const float walkSpeed = 1.3f; // Walking speed of the soldier
		const float runSpeed = 2.5f; // Running speed of the soldier
		const float formationWalkSpeed = 1.1f; // Walking speed of the formation

		Soldier* soldier = soldierNode->GetComponent<Soldier>();
		CrowdAgent* agent = soldierNode->GetComponent<CrowdAgent>();

		int assignedSpotId = assignment_.GetSpotForSoldier(soldierNode);
		FormationSpot assignedSpotWorld = worldSpaceFormation_->GetSpot(assignedSpotId);
		FormationSpot assignedSpotUnit = rotatedFormation->GetSpot(assignedSpotId);

		// Calculate the desired position by getting a position nearby the unit but nearer the ultimate target
		Vector3 desiredEndPosition = navMesh->FindNearestPoint(assignedSpotUnit.position + unitDesiredMovement, Vector3(0.0f, 1000.0f, 0.0f));
		Vector3 desiredCurrentPosition = navMesh->FindNearestPoint(assignedSpotUnit.position, Vector3(0.0f, 1000.0f, 0.0f));

		Vector3 targetOnWay = desiredCurrentPosition + unitDesiredMovementDirection * formationWalkSpeed * 0.5f;

		// If close enough, just skip to the end position instead of moving in formation
		Vector3 targetPosition = (unitDesiredMovement.LengthSquared() < 0.8f * 0.8f) ? desiredEndPosition : navMesh->FindNearestPoint(targetOnWay, Vector3(0.0f, 1000.0f, 0.0f));

		float distanceLeftSquared = (agent->GetPosition() - desiredEndPosition).LengthSquared();

		float distFromDesiredCurrentPosition = (agent->GetPosition() - desiredCurrentPosition).Length();
		float maxSpeed = Min(distFromDesiredCurrentPosition + formationWalkSpeed * 2.0f, runSpeed);

		//float maxSpeed = 4.0f;
		maxSpeed = (distanceLeftSquared < maxSpeed * maxSpeed) ? formationWalkSpeed : maxSpeed;

		agent->SetMaxSpeed(maxSpeed);
		// Todo: implement a faster version of this in detourcrowd? or use target velocity
		soldier->coordinatorPosition = targetPosition;
		soldier->coordinatorDirection = assignedSpotWorld.direction;
	}
}

void FormationCoordinator::CreateUnitSpaceFormation() {
	Vector3 unitPosition = node_->GetComponent<Unit>()->GetPosition();
	unitSpaceFormation_ = SharedPtr<Formation>(new Formation(context_));

	for (unsigned i = 0; i < worldSpaceFormation_->GetSpotCount(); i++) {
		FormationSpot currWorldSpaceSpot = worldSpaceFormation_->GetSpot(i);

		FormationSpot unitSpaceSpot = currWorldSpaceSpot;
		unitSpaceSpot.position = currWorldSpaceSpot.position - worldSpaceFormation_->GetCentre();
		unitSpaceSpot.direction = currWorldSpaceSpot.direction;

		unitSpaceFormation_->AddSpot(unitSpaceSpot);
	}
}

SharedPtr<Formation> FormationCoordinator::GetRotatedFormation(SharedPtr<Formation> formationToRotate) {
	Vector3 unitPosition = node_->GetComponent<Unit>()->GetPosition();
	auto formation = SharedPtr<Formation>(new Formation(context_));

	for (unsigned i = 0; i < formationToRotate->GetSpotCount(); i++) {
		FormationSpot unitSpaceSpot = formationToRotate->GetSpot(i);

		FormationSpot rotatedSpot = unitSpaceSpot;

		rotatedSpot.position = unit_->GetPosition() + GetRotatedFormationPoint(unitSpaceSpot.position);
		rotatedSpot.direction = GetRotatedFormationPoint(unitSpaceSpot.direction);

		formation->AddSpot(rotatedSpot);
	}

	return formation;
}

void FormationCoordinator::UpdateFormationRotation()
{
	// Only use rotation if we are far enough from our final position to justify it
	const float distanceToStartWheeling = 2.5f;
	const float distanceToFinishWheeling = 0.5f; // We need an epsilon too, as towardsFinalPosition never truly hits perfect 0

	if (!worldSpaceFormation_ || !unitSpaceFormation_) {
		return;
	}

	Vector3 alongFrontRowOfFormation = worldSpaceFormation_->GetEndPoint() - worldSpaceFormation_->GetStartPoint();
	Vector3 forwards = VecUtils::flatten(-Vector3::UP.CrossProduct(alongFrontRowOfFormation));
	Vector3 towardsFinalPosition = VecUtils::flatten(worldSpaceFormation_->GetCentre()) - VecUtils::flatten(unit_->GetPosition());
	float towardsFinalPositionDistance = towardsFinalPosition.Length();

	Quaternion r = towardsFinalPositionDistance > distanceToFinishWheeling ? Quaternion(forwards, towardsFinalPosition) : Quaternion::IDENTITY;

	float angle = r.YawAngle();
	const float angleLimit = 70.0f;
	// If it's an extreme angle, then just reassign instead of wheel
	if ((angle > angleLimit || angle < -angleLimit) && towardsFinalPositionDistance <= distanceToStartWheeling) {
		// To avoid reassigning every tick
		if (wheelAmount_ != Quaternion::IDENTITY) {
			assignment_ = HungarianAlgorithmSpotAssignment(*node_->GetComponent<Unit>(), *worldSpaceFormation_);
		}

		wheelAmount_ = Quaternion::IDENTITY;
		return;
	}

	// Calculate a rotate amount based on how far we have left to go
	float rotateAmount = (towardsFinalPositionDistance - distanceToFinishWheeling) / (distanceToStartWheeling - distanceToFinishWheeling);
	rotateAmount = Clamp(rotateAmount, 0.0f, 1.0f); // 0 = no rotation, 1 = facing perfectly towards final position

	// Now correct rotation by rotateAmount
	wheelAmount_ = Quaternion::IDENTITY.Slerp(r, rotateAmount);
}


Vector3 FormationCoordinator::GetRotatedFormationPoint(Vector3 p) {
	return wheelAmount_ * p;
}