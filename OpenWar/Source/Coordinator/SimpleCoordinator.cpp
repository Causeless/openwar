#include "Coordinator.h"
#include "../VecUtils.h"
#include "../Unit.h"
#include "../Soldier.h"
#include "../SpotAssignment.h"
#include "../Formation.h"
#include "../SyncedRandom.h"

#include <Urho3D/Core/Context.h>
#include <Urho3D/Navigation/NavigationMesh.h>
#include <Urho3D/Navigation/CrowdAgent.h>
#include <Urho3D/Scene/Node.h>

SimpleCoordinator::SimpleCoordinator(Context* context) : Coordinator(context)
{

}

void SimpleCoordinator::RegisterObject(Context* context)
{
	context->RegisterFactory<SimpleCoordinator>("COORDINATOR");
}

void SimpleCoordinator::SetFormation(Formation* formation) {
	unit_ = node_->GetComponent<Unit>();
	worldSpaceFormation_ = SharedPtr<Formation>(formation);

	assignment_ = HungarianAlgorithmSpotAssignment(*node_->GetComponent<Unit>(), *worldSpaceFormation_);
	//ticksUntilAssignRefresh_ = (SyncedRandom::Rand() % 50);
}

void SimpleCoordinator::SetFormationNoReassign(Formation* formation) {
	unit_ = node_->GetComponent<Unit>();
	worldSpaceFormation_ = SharedPtr<Formation>(formation);
}

void SimpleCoordinator::UpdateSoldierTargets(float dt)
{
	if (worldSpaceFormation_) {
		//ticksUntilAssignRefresh_--;

		auto navMesh = node_->GetParentComponent<NavigationMesh>();

		for (auto soldierNode : unit_->GetSoldiers()) {
			Soldier* soldier = soldierNode->GetComponent<Soldier>();
			CrowdAgent* agent = soldierNode->GetComponent<CrowdAgent>();

			int assignedSpotId = assignment_.GetSpotForSoldier(soldierNode);
			FormationSpot assignedSpotWorld = worldSpaceFormation_->GetSpot(assignedSpotId);

			float distanceLeftSquared = (agent->GetPosition() - assignedSpotWorld.position).LengthSquared();

			float maxSpeed = (distanceLeftSquared < 1.0f * 1.0f) ? 1.5f : 1.6f;

			Vector3 targetPosition = assignedSpotWorld.position;
			Vector3 targetPositionOnMesh = navMesh->FindNearestPoint(targetPosition, Vector3(1.0f, 1000.0f, 1.0f));

			agent->SetMaxSpeed(maxSpeed);
			// Todo: implement a faster version of this in detourcrowd
			soldier->coordinatorPosition = targetPositionOnMesh;
			soldier->coordinatorDirection = assignedSpotWorld.direction;
		}
	}
}