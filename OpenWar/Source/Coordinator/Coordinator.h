#pragma once

#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Scene/Component.h>

#include "../SpotAssignment.h"
#include "../Formation.h"

using namespace Urho3D;

class Unit;

// The Coordinator handles keeping a unit's soldiers coordinated and in formation (or not)
class Coordinator : public Component {
	URHO3D_OBJECT(Coordinator, Component);

public:
	Coordinator(Context* context) : Component(context) { worldSpaceFormation_ = 0; }
	static void RegisterObject(Context* context) {};

	virtual void SetFormation(Formation* formation) = 0;
	virtual void SetFormationNoReassign(Formation* formation) = 0;
	Formation* GetFormation() { return worldSpaceFormation_; }

	virtual void UpdateSoldierTargets(float dt) = 0;

	SpotAssignment* GetAssignment() { return &assignment_; }

protected:
	// We use CustomFormation so we can manually create spots
	SharedPtr<Formation> worldSpaceFormation_;

	SpotAssignment assignment_;
	Unit* unit_;

	int ticksUntilAssignRefresh_ = 0;
};

class SimpleCoordinator : public Coordinator {
	URHO3D_OBJECT(SimpleCoordinator, Coordinator);

public:
	SimpleCoordinator(Context* context);
	static void RegisterObject(Context* context);

	virtual void SetFormation(Formation* formation);
	virtual void SetFormationNoReassign(Formation* formation);
	virtual void UpdateSoldierTargets(float dt);
};

class FormationCoordinator : public Coordinator {
	URHO3D_OBJECT(FormationCoordinator, Coordinator);

public:
	FormationCoordinator(Context* context);
	static void RegisterObject(Context* context);

	virtual void SetFormation(Formation* formation);
	virtual void SetFormationNoReassign(Formation* formation);
	virtual void UpdateSoldierTargets(float dt);

private: 
	void UpdateFormationRotation();
	Vector3 GetRotatedFormationPoint(Vector3 p);
	void CreateUnitSpaceFormation();
	SharedPtr<Formation> GetRotatedFormation(SharedPtr<Formation> formationToRotate);

	SharedPtr<Formation> unitSpaceFormation_;
	Quaternion wheelAmount_;
};

class LocalFormationCoordinator : public Coordinator {
	URHO3D_OBJECT(LocalFormationCoordinator, Coordinator);

public:
	LocalFormationCoordinator(Context* context);
	static void RegisterObject(Context* context);

	virtual void SetFormation(Formation* formation);
	virtual void SetFormationNoReassign(Formation* formation);
	virtual void UpdateSoldierTargets(float dt);

private:
	Vector3 GetDesiredPosition(Formation* formation, unsigned spotId);
	void UpdateFormationRotation();
	Vector3 GetRotatedFormationPoint(Vector3 p);
	void CreateUnitSpaceFormation();
	void SeedRandomOffsets();
	Vector3 OffsetForSoldier(unsigned id);
	SharedPtr<Formation> GetRotatedFormation(SharedPtr<Formation> formationToRotate);
	
	SharedPtr<Formation> unitSpaceFormation_;
	PODVector<Vector3> randomOffsets_;

	Quaternion wheelAmount_;
};