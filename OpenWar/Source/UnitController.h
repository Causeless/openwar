#pragma once

#include "Unit.h"
#include "Formation.h"

using namespace Urho3D;

// UnitController selects units and gives them commands
class UnitController : public Component {
	URHO3D_OBJECT(UnitController, Component);

public:
	UnitController(Context* context);
	static void RegisterObject(Context* context);

	void SetFormation(Vector3 startPoint, Vector3 endPoint);

protected:
	/// Handle node being assigned.
	virtual void OnNodeSet(Node* node);
};