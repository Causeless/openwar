//
// Copyright (c) 2008-2017 the Urho3D project.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#pragma once

#include <Urho3D/Engine/Application.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Network/Connection.h>

// All Urho3D classes reside in namespace Urho3D
using namespace Urho3D;

struct Client;

static const int TICK_BATCH_SIZE = 3;

static const unsigned short SERVER_PORT = 2345;
static const StringHash E_STEP("Step");
static const StringHash E_CLIENTSTEP("ClientStep");
static const StringHash P_TICK("Tick");
static const StringHash P_CHECKSUM("Checksum");

class LockstepServer : public LogicComponent
{
    // Enable type information.
    URHO3D_OBJECT(LockstepServer, LogicComponent);

public:
    /// Construct.
	LockstepServer(Context* context);
	static void RegisterObject(Context* context_);

    virtual void Start() override;

	void StartServer();
    void StopServer();

private:
	void HandleClientStep(StringHash eventType, VariantMap& eventData);
	void HandleClientConnected(StringHash eventType, VariantMap& eventData);
	void HandleClientDisconnected(StringHash eventType, VariantMap& eventData);
	void HandleNetworkMessage(StringHash eventType, VariantMap& eventData);

	bool ChecksumMismatch(Vector<Client> &clientsVector);

	void UpdateTick();

	HashMap<Connection*, Client> clients_;

	int simToPhysicsTick_ = TICK_BATCH_SIZE; // The physics tick clients should simulate to before stopping and waiting for network update (i.e new inputs)
};