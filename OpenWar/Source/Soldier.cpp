#include "Soldier.h"
#include "Unit.h"
#include "VecUtils.h"
#include "SyncedRandom.h"

#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Navigation/NavigationEvents.h>
#include <Urho3D/Navigation/CrowdAgent.h>
#include <Urho3D/Graphics/AnimatedModel.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Graphics/AnimationController.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Navigation/CrowdAgent.h>
#include <Urho3D/Navigation/NavigationMesh.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Physics/PhysicsEvents.h>

#include <Urho3D/Graphics/ParticleEmitter.h>
#include <Urho3D/Graphics/ParticleEffect.h>

#include <Urho3D/IO/Log.h>

//static const char* animSet_->attack = "Models/RSII_Hastati_Attack.ani";
//static const char* animSet_->defend = "Models/RSII_Hastati_Defend.ani";
//static const char* animSet_->idle = "Models/RSII_Hastati_Ready.ani";
//static const char* animSet_->idle = "Models/RSII_Hastati_Idle.ani";
//static const char* animSet_->run = "Models/RSII_Hastati_Run.ani";
//static const char* animSet_->walk = "Models/RSII_Hastati_Advance.ani";
//static const char* animSet_->walk = "Models/RSII_Hastati_Walk.ani";

//static const char* animSet_->idle = "Models/hoplite_Ready.ani";
//static const char* animSet_->run = "Models/hoplite_Run.ani";
//static const char* animSet_->walk = "Models/hoplite_Advance.ani";

static const float ANIM_ATTACK_LENGTH = 0.9f;

Soldier::Soldier(Context* context) : Component(context) {
	
}

Soldier::~Soldier() {

}

void Soldier::RegisterObject(Context* context)
{
	context->RegisterFactory<Soldier>("Soldier");
}

void Soldier::Hurt(Node* attacker)
{
	UnitStats myStats = node_->GetParentComponent<Unit>()->GetUnitStats();

	Unit* myUnit = node_->GetParentComponent<Unit>();

	AnimationController* animCtrl = node_->GetComponent<AnimationController>();
	animCtrl && animCtrl->PlayExclusive(animSet_->defend, 0, false);

	// Percent chance to block attack
	float rand = SyncedRandom::Random();
	if (rand <= myStats.defence_) {
		return;
	}

	if (myUnit->Hurt()) {
		Kill();
		myUnit->SetDirty();
	}
}

void Soldier::Push(float timeStep, CrowdAgent* attacker, float amount)
{
	// TODO; find some way of applying pushes to be framerate independent
	// Pushes last only one frame, so a push at a lower framerate has more effect than at a lower framerate
	Vector3 dirAwayFromAttacker = (agent_->GetPosition() - attacker->GetPosition()).Normalized();
	agent_->SetTargetPosition(agent_->GetPosition() + dirAwayFromAttacker * amount * timeStep);
}

void Soldier::SetToAttack(Node* target)
{
	if (target == combatTarget_) {
		return;
	}

	combatTarget_ = target;

	if (combatTarget_) {
		Node* otherAttacker = combatTarget_->GetComponent<Soldier>()->combatTarget_;
		CrowdAgent* otherAttackerAgent = nullptr;

		if (otherAttacker && otherAttacker != node_) {
			otherAttackerAgent = otherAttacker->GetComponent<CrowdAgent>();
		}

			if (otherAttackerAgent) {
			// Closer one is the one that defends
			CrowdAgent* attackedAgent = combatTarget_->GetComponent<CrowdAgent>();

			float myDistance = (attackedAgent->GetPosition() - agent_->GetPosition()).LengthSquared();
			float otherDistance = (otherAttackerAgent->GetPosition() - agent_->GetPosition()).LengthSquared();

			combatTarget_->GetComponent<Soldier>()->combatTarget_ = myDistance < otherDistance ? node_ : otherAttacker;
		}
		else {
			combatTarget_->GetComponent<Soldier>()->combatTarget_ = node_;
		}
	}
}

void Soldier::CheckAttackFinished(float timeStep)
{
	attackAnimTimeLeft_ -= timeStep;

	if (attackAnimTimeLeft_ <= 0.0f) {
		// Reset attack anim lengths
		attackAnimTimeLeft_ = ANIM_ATTACK_LENGTH;
		isCurrentlyAttacking_ = false;

		if (!combatTarget_) {
			return;
		}

		CrowdAgent* attackedAgent = combatTarget_->GetComponent<CrowdAgent>();
		Soldier* attackedSoldier = combatTarget_->GetComponent<Soldier>();

		if (!attackedAgent || !attackedSoldier) {
			combatTarget_ = nullptr;
			return;
		}

		UnitStats myStats = node_->GetParentComponent<Unit>()->GetUnitStats();
		UnitStats theirStats = combatTarget_->GetParentComponent<Unit>()->GetUnitStats();

		combatTarget_ = nullptr;

		// Push them back
		// We do this at the end of the hit instead of the start (as with pushback) because it looks more random and natural
		attackedSoldier->Push(timeStep, agent_, myStats.pushback_ / theirStats.pushbackResistance_);

		// Determine whether we hit or missed
		if (SyncedRandom::Random() <= myStats.attack_) {
			attackedSoldier->Hurt(node_);
		}
	}
}

void Soldier::AttemptHitTarget(float timeStep, Node* target)
{
	// If I'm already attacking, I can't attack two people at once! so surrounded soldiers have a disadvantage as expected
	if (isCurrentlyAttacking_ || timeUntilNextAttackAttempt_ > 0.0f) {
		return;
	}

	// If they're out of range, I can't attack
	CrowdAgent* attackedAgent = target->GetComponent<CrowdAgent>();
	if (!IsInAttackRange(attackedAgent)) {
		return;
	}

	combatTarget_ = target;

	UnitStats myStats = node_->GetParentComponent<Unit>()->GetUnitStats();
	UnitStats theirStats = target->GetParentComponent<Unit>()->GetUnitStats();
	Soldier* attackedSoldier = target->GetComponent<Soldier>();

	// Add, instead of setting, to make up for lost time. If this is -1, for example, then time has already passed allowing us to attack, just we weren't updated yet
	timeUntilNextAttackAttempt_ += myStats.attackDelay_;

	// Step forwards
	Push(timeStep, attackedAgent, -(myStats.pushForwards_ / theirStats.pushForwardsResistance_));

	isCurrentlyAttacking_ = true;

	AnimationController* animCtrl = node_->GetComponent<AnimationController>();
	if (animCtrl)
	{
		animCtrl->PlayExclusive(animSet_->attack, 0, false, 1.0f);
		animCtrl->SetSpeed(animSet_->attack, 0.8f);
	}
}

void Soldier::MoveToAttackTarget(float timeStep)
{
	CrowdAgent* attackedAgent = combatTarget_->GetComponent<CrowdAgent>();
	if (!attackedAgent) {
		// Something killed our target, so cancel
		combatTarget_ = nullptr;
		return;
	}

	UnitStats myStats = node_->GetParentComponent<Unit>()->GetUnitStats();
	UnitStats theirStats = combatTarget_->GetParentComponent<Unit>()->GetUnitStats();

	Vector3 difference = (attackedAgent->GetPosition() - coordinatorPosition);
	float differenceLen = difference.Length();

	float distanceToCombatMax = differenceLen - myStats.combatDistanceMax_;
	float distanceToCombatMin = differenceLen - myStats.combatDistanceMin_;
	float distanceToAttack = differenceLen - myStats.attackDistance_ * 0.95f /* epsilon */;

	// If we're too far from our formation spot, go back to it
	if ((coordinatorPosition - agent_->GetPosition()).LengthSquared() > myStats.moveToAttackDistance_ * myStats.moveToAttackDistance_) {
		agent_->SetTargetPosition(coordinatorPosition);
	} 
	// Otherwise move in to combat distance if we're on our cooldown
	else if (timeUntilNextAttackAttempt_ > 0.0f && distanceToCombatMax > 0.0f) {
		// Find a point that is as far out of the formation we can, that is outside min combat range
		Vector3 offset = difference.Normalized() * Min(distanceToCombatMax, myStats.moveToAttackDistance_);
		agent_->SetTargetPosition(coordinatorPosition + offset);
	}
	else if (timeUntilNextAttackAttempt_ > 0.0f && distanceToCombatMin < 0.0f) {
		// Find a point that is as far out of the formation we can, that is outside min combat range
		Vector3 offset = difference.Normalized() * Min(distanceToCombatMin, myStats.moveToAttackDistance_);
		agent_->SetTargetPosition(coordinatorPosition + offset);
	}
	// Otherwise move into attack
	else if (timeUntilNextAttackAttempt_ <= 0.0f && distanceToAttack > 0.0f) {
		// Find a point that is as far out of the formation we can, to go to be in attack range
		Vector3 offset = difference.Normalized() * Min(distanceToAttack, myStats.moveToAttackDistance_);
		agent_->SetTargetPosition(coordinatorPosition + offset);
	}

	if (IsInAttackRange(attackedAgent)) {
		InitiateCombat(timeStep);
	}
}

void Soldier::InitiateCombat(float timeStep)
{
	// Todo. need to change this so it won't be called, I guess, when the other attacking soldier has already called their init
	if (timeUntilNextAttackAttempt_ > 0.0f) {
		return;
	}

	UnitStats myStats = node_->GetParentComponent<Unit>()->GetUnitStats();
	UnitStats theirStats = combatTarget_->GetParentComponent<Unit>()->GetUnitStats();

	float totalInitiative = myStats.initiative_ + theirStats.initiative_;
	float chance = myStats.initiative_ / totalInitiative;

	float rand = SyncedRandom::Random();

	if (rand < chance) {
		// I win! Try to hit them.
		AttemptHitTarget(timeStep, combatTarget_);
	}
	else {
		// I lose! Now they attempt to hit me :(
		Soldier* them = combatTarget_->GetComponent<Soldier>();
		them->AttemptHitTarget(timeStep, node_);
	}
}

bool Soldier::IsInAttackRange(CrowdAgent* attackedAgent)
{
	UnitStats stats = node_->GetParentComponent<Unit>()->GetUnitStats();
	return (attackedAgent->GetPosition() - agent_->GetPosition()).LengthSquared() < stats.attackDistance_*stats.attackDistance_;
}

void Soldier::OnNodeSet(Node* node)
{
	if (node) {
		SubscribeToEvent(E_PHYSICSPRESTEP, URHO3D_HANDLER(Soldier, UpdateSim));
		SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(Soldier, UpdateRender));
	}
}

void Soldier::UpdateSim(StringHash eventType, VariantMap& eventData)
{
	using namespace PhysicsPreStep;

	// Take the frame time step, which is stored as a float
	float timeStep = eventData[P_TIMESTEP].GetFloat();

	if (timeUntilNextAttackAttempt_ > 0.0f) {
		timeUntilNextAttackAttempt_ -= timeStep;
	}

	currSpeed_ = agent_->GetActualVelocity().Length();
	HandleMovementAnimation(timeStep);

	if (combatTarget_) {
		MoveToAttackTarget(timeStep);
	}
	else {
		agent_->SetTargetPosition(coordinatorPosition);
		//agent_->SetTargetVelocity(coordinatorPosition - agent_->GetPosition());
	}

	if (isCurrentlyAttacking_) {
		CheckAttackFinished(timeStep);
	}
}

void Soldier::UpdateRender(StringHash eventType, VariantMap& eventData)
{
	using namespace Update;

	// Take the frame time step, which is stored as a float
	float timeStep = eventData[P_TIMESTEP].GetFloat();

	Vector3 velocity = agent_->GetActualVelocity();
	float speed = currSpeed_ * timeStep;

	const float turnSpeed = 1.5f;

	if (combatTarget_) {
		Vector3 targetDirection = VecUtils::flatten(combatTarget_->GetPosition() - node_->GetPosition());
		node_->SetRotation(node_->GetRotation().Slerp(Quaternion(Vector3::FORWARD, targetDirection), timeStep*turnSpeed));
	}
	else if ((agent_->GetPosition() - coordinatorPosition).LengthSquared() < 0.25f*0.25f && currSpeed_ < 0.25f) {
		node_->SetRotation(node_->GetRotation().Nlerp(Quaternion(Vector3::FORWARD, coordinatorDirection), timeStep*turnSpeed));
	}
	else {
		// Face the direction of its velocity but moderate the turning speed based on the speed ratio and timeStep
		node_->SetRotation(node_->GetRotation().Nlerp(Quaternion(Vector3::FORWARD, velocity), speed*turnSpeed));
	}
}

void Soldier::HandleMovementAnimation(float timeStep)
{
	// Set height as RTW models have the root at 0,0,0; we need feet at 0,0,0
	/*AnimatedModel* model = node_->GetComponent<AnimatedModel>();
	if (model)
	{
		/*Bone* root = model->GetSkeleton().GetRootBone();
		Vector3 currentPos = root->node_->GetPosition();
		float height = model->GetWorldBoundingBox().HalfSize().y_;
		Vector3 desiredPos = Vector3(0.0f, height - 0.1f, 0.0f);

		model->GetSkeleton().GetRootBone()->node_->SetPosition(currentPos.Lerp(desiredPos, 2.0f*timeStep));
	}
	*/

	// Only Soldier agent has animation controller
	AnimationController* animCtrl = node_->GetComponent<AnimationController>();
	if (animCtrl)
	{
		if (animCtrl->IsPlaying(animSet_->attack) || animCtrl->IsPlaying(animSet_->defend)) {
			if (animCtrl->IsAtEnd(animSet_->defend)) {
				animCtrl->Stop(animSet_->defend);
				animCtrl->PlayExclusive(animSet_->idle, 0, true, 1.0f);
			}
			
			if (animCtrl->IsAtEnd(animSet_->attack)) {
				animCtrl->Stop(animSet_->attack);
				animCtrl->PlayExclusive(animSet_->idle, 0, true, 1.0f);
			}

			return;
		}

		String currentAni = String();

		if (currSpeed_ > 1.8f) {
			currentAni = animSet_->run;
		}
		else {
			currentAni = animSet_->walk;
		}

		if (animCtrl->IsPlaying(currentAni))
		{
			float animSpeed = currSpeed_;
			// If we're moving backwards, reverse the currspeed
			if (agent_->GetActualVelocity().Angle(GetNode()->GetDirection()) > 90.0f) {
				animSpeed *= -1.0f;
			}

			// Throttle the animation speed based on agent speed ratio (ratio = 1 is full throttle)
			animCtrl->SetSpeed(animSet_->run, animSpeed *0.1f);
			animCtrl->SetSpeed(animSet_->walk, animSpeed *0.75f);
			//animCtrl->SetSpeed(animSet_->walk, rawSpeed*0.25f);
		}
		else if (!currentAni.Empty()) {
			animCtrl->PlayExclusive(currentAni, 0, true, 0.5f);

			// Add some random variance to start to stop looking too mechanical
			animCtrl->SetTime(currentAni, RandStandardNormal()*2.0f);
		}

		// If speed is too low then stop the animations
		if (currSpeed_ < -0.18f) {
			animCtrl->PlayExclusive(animSet_->idle, 0, true, 1.0f);
			animCtrl->SetSpeed(animSet_->idle, 0.15f);
			//animCtrl->Stop(animSet_->walk, 0.5f);
			//animCtrl->Stop(animSet_->run, 0.5f);
		}
	}
}

void Soldier::Kill()
{
	node_->GetParent()->GetComponent<Unit>()->SetDirty();
	node_->RemoveAllComponents();

	// Todo - spawn a dead body, play an anim, whatever
}

void Soldier::Spawn(SoldierModel& model, SoldierAnimSet* animSet)
{
	ResourceCache* cache = GetSubsystem<ResourceCache>();

	// Create a CrowdAgent component and set its height and realistic max speed/acceleration. Use default radius
	agent_ = node_->CreateComponent<CrowdAgent>(LOCAL);
	agent_->SetRadius(0.29f);
	agent_->SetHeight(1.75f);
	agent_->SetNavigationQuality(NAVIGATIONQUALITY_OPENWAR);
	agent_->SetNavigationPushiness(NAVIGATIONPUSHINESS_OPENWAR);
	//Max speed is set by coordinator agent->SetMaxSpeed(4.0f);
	agent_->SetMaxAccel(3.5f);

	coordinatorPosition = agent_->GetPosition();
	coordinatorDirection = node_->GetDirection();
	agent_->SetTargetPosition(coordinatorPosition);

	// I fucking hate this fucking piece of shit engine which won't render animated models in fucking debug fuck
	//StaticModel* staticModel = node_->CreateComponent<StaticModel>(LOCAL);
	//staticModel->SetModel(cache->GetResource<Model>("Models/Cylinder.mdl"));
	//node_->SetScale(Vector3(0.5f, 1.0f, 0.5f));

	AnimatedModel* modelObject = node_->CreateComponent<AnimatedModel>(LOCAL);
	modelObject->SetViewMask(0x00000002);

	modelObject->SetModel(cache->GetResource<Model>(model.model));
	modelObject->SetMaterial(cache->GetResource<Material>(model.material));

	modelObject->SetCastShadows(false);
	//modelObject->SetShadowDistance(50.0f);
	AnimationController* animCtrl = node_->CreateComponent<AnimationController>(LOCAL);

	Bone* root = modelObject->GetSkeleton().GetRootBone();
	if (root) {
		float height = modelObject->GetWorldBoundingBox().HalfSize().y_;
		root->animated_ = false;
		root->node_->SetPosition(Vector3(0.0f, height - 0.1f, 0.0f));
	}

	// Hide pilum
	Bone* pilum = modelObject->GetSkeleton().GetBone("primary weapon");
	if (pilum) {
		// Set node scale before it is ever animated
		pilum->node_->SetScale(Vector3::ZERO);

		// Set bone scale for when it becomes animated
		pilum->initialScale_ = Vector3::ZERO;
	}

	animSet_ = animSet;
	attackAnimTimeLeft_ = ANIM_ATTACK_LENGTH;
}