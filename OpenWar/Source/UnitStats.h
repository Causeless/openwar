#pragma once

// Brief overview of the system and things to consider:

// First, a soldier attempts to enter attackRange of their enemy. This is controlled primarily by the moveToAttackDistance_
// This value represents how far the enemy will move out of formation to attack, and therefore the general aggressiveness of the unit as well as the stability of their formation
// If this value is high, soldiers are more likely to surround their enemy by moving out of formation, 
// while if it's low, they'll stay in formation even if it means not taking advantange of a flank

// Once in range, we initiate combat with the enemy
// The iniative value determines how likely the soldier is to "win" the initiative; if they win, they get to attack the enemy, and if they lose, the enemy gets to attack them instead
// Note the following two points: even if the we lose the initiative, the enemy cannot attack if they're already attacking another soldier, or if the enemy is outside attack range
// or if they are currently in their attack cooldown
// In this case, losing the initiative effectively means being a draw (and nothing happening) instead of a loss
// This gives soldiers with lower attack range, that are crowded by multiple enemies, or have lower attack speed, a relative disadvantage

// After initative is calculated, the winning side will swing to attack, and will push the enemy (regardless of whether the attack hits).
// We  set the attackDelay into effect, and will not attempt to attack an enemy until this time has passed
// The distance to push is controlled by two components: push forwards, which is how much we step forwards, and pushback, which is how far we push the enemy back
// For the most part, push forwards has more effect than push back, as if we don't step forwards to take advantage of the gap the enemy will quickly move forwards to fill it
// Note that formation tightness is important here. If the unit is in a tight formation, then even if they get pushed back, they may not be able to move too far due to an ally behind them

// When we swing to attack an enemy, the attack value is rolled, generating a random chance of hitting the enemy.
// If the hit loses, nothing will happen, whereas if it succeeds, the enemy will play a stumble animation and then roll their defence value
// If they win the defence roll, they block the hit without taking any damage
// If they lose the defence roll, they are hurt - they tell the unit, which decrements the unit's universal HP pool accordingly
// Finally, if the HP has been reduced enough (by healthPointsPerSoldier), the soldier that was just attacked is killed.

struct UnitStats {
	// This HP value is added up for a total HP on the unit level, which will kill a soldier within when it's HP is reduced by this amount.
	// Think of this value as a scale of extra "virtual" soldiers for each real soldier in the unit
	int healthPointsPerSoldier_ = 4;
	
	// Initiative is how often soldiers in this unit will be attacker rather than defender when combat is initiated. 
	// Can be any positive value. This is compared with enemy initiative, so if both are equal, it's 50/50. If our initative is 200 and their's is 100, then it's 2/3, and so on.
	float initiative_ = 1.0f;

	// Attack is how likely our attack is to hit the enemy and not miss (0.0f - 1.0f)
	float attack_ = 0.3f;

	// How close to the enemy to move before we can attempt to attack
	float attackDistance_ = 1.8f;

	// How close to the enemy to aim to be while our attack cooldown in in effect
	float combatDistanceMin_ = 1.4f;
	float combatDistanceMax_ = 2.5f;

	// Attack delay is how often we can attack, in seconds
	float attackDelay_ = 5.0f;

	// Defence is how likely we are to block a successful hit (0.0f - 1.0f)
	float defence_ = 0.7f;

	// Pushback is how far we push the enemy when we successfully hit them
	float pushback_ = 2.5f;

	// Pushback resistance is how well we resist an enemy's pushback (they push by their pushback divided by our pushbackResistance)
	float pushbackResistance_ = 1.0f;

	// Push forwards is how far forwards we'll attempt to move after successfully hitting the enemy
	// If push back is how far back we push the enemy, push forwards is how aggressively we try to take that newly gained space
	float pushForwards_ = 1.5f;

	// Pushback resistance is how well we resist an enemy's pushforwards
	float pushForwardsResistance_ = 1.0f;

	// How far a soldier is willing to move out of formation to attack
	float moveToAttackDistance_ = 0.3f;

	// The higher this value, the more disorderly the unit is
	float formationRandomness_ = 0.25f;

	// How close to their formation spot a soldier needs to be before they think "fuck it, close enough"
	// This doesn't really work right now. need to think...
	float formationTolerance_ = 0.0f;
};