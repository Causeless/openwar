//
// Copyright (c) 2008-2017 the Urho3D project.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include <Urho3D/Core/CoreEvents.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Graphics/AnimatedModel.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/DebugRenderer.h>
#include <Urho3D/Graphics/Graphics.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/Zone.h>
#include <Urho3D/Graphics/Geometry.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Input/InputEvents.h>
#include <Urho3D/Navigation/CrowdAgent.h>
#include <Urho3D/Navigation/NavigationMesh.h>
#include <Urho3D/Navigation/Navigable.h>
#include <Urho3D/Navigation/NavigationEvents.h>
#include <Urho3D/Navigation/Obstacle.h>
#include <Urho3D/Navigation/OffMeshConnection.h>
#include <Urho3D/Physics/PhysicsWorld.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Physics/CollisionShape.h>
#include <Urho3D/Physics/PhysicsEvents.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Graphics/Terrain.h>

#include "Urho3D/Core/Profiler.h"

#include <Urho3D/Network/Network.h>
#include <Urho3D/Network/NetworkEvents.h>

#include <Urho3D/UI/Button.h>
#include <Urho3D/UI/Font.h>
#include <Urho3D/UI/Text.h>
#include <Urho3D/UI/LineEdit.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/UI/UIEvents.h>

#include "LockstepServer.h"
#include "Soldier.h"
#include "Unit.h"
#include "UnitController.h"
#include "UnitSelector.h"
#include "Formation.h"
#include "VecUtils.h"
#include "Coordinator/Coordinator.h"
#include "Command.h"

#include "OpenWar.h"

#include <Urho3D/DebugNew.h>

URHO3D_DEFINE_APPLICATION_MAIN(CrowdNavigation)

CrowdNavigation::CrowdNavigation(Context* context) :
    Sample(context),
    drawDebug_(false)
{
	Formation::RegisterObject(context);
	GridFormation::RegisterObject(context);
	HexFormation::RegisterObject(context);

	Command::RegisterObject(context);
	MovementCommand::RegisterObject(context);
	AttackCommand::RegisterObject(context);

	Coordinator::RegisterObject(context);
	SimpleCoordinator::RegisterObject(context);
	FormationCoordinator::RegisterObject(context);
	LocalFormationCoordinator::RegisterObject(context);

	UnitController::RegisterObject(context);
	UnitSelector::RegisterObject(context);
	CommandApplier::RegisterObject(context);

	Unit::RegisterObject(context);

	Soldier::RegisterObject(context);

	LockstepServer::RegisterObject(context);
}

void CrowdNavigation::Start()
{
    // Execute base class startup
    Sample::Start();

    // Create the scene content
    CreateScene();

    // Create the UI content
    CreateUI();

    // Setup the viewport for displaying the scene
    SetupViewport();

    // Hook up to the frame update and render post-update events
    SubscribeToEvents();

    // Set the mouse mode to use in the sample
    Sample::InitMouseMode(MM_ABSOLUTE);
}

static const SoldierModel HASTATI_MODEL = SoldierModel{
	"Models/RSII_Hastati.mdl",
	"Materials/RSII_Hastati.xml"
};

static const SoldierAnimSet HASTATI_ANIM = SoldierAnimSet{
	"Models/RSII_Hastati_Attack.ani",
	"Models/RSII_Hastati_Defend.ani",
	"Models/RSII_Hastati_Ready.ani",
	"Models/RSII_Hastati_Advance.ani",
	"Models/RSII_Hastati_Run.ani"
};

static const SoldierModel HOPLITE_MODEL = SoldierModel{
	"Models/hoplite.mdl",
	"Materials/hoplite.xml"
};

static const SoldierAnimSet HOPLITE_ANIM = SoldierAnimSet{
	"Models/hoplite_Attack.ani",
	"Models/hoplite_Defend.ani",
	"Models/hoplite_Ready.ani",
	"Models/hoplite_Advance.ani",
	"Models/hoplite_Run.ani"
};

void CrowdNavigation::CreateScene()
{
	const float precisionOffset = 0.0f;

	ResourceCache* cache = GetSubsystem<ResourceCache>();

	scene_ = new Scene(context_);
	scene_->SetTimeScale(0.0f);

	scene_->CreateComponent<LockstepServer>();

	// Create octree, use default volume (-1000, -1000, -1000) to (1000, 1000, 1000)
	// Also create a DebugRenderer component so that we can draw debug geometry
	scene_->CreateComponent<Octree>(LOCAL);
	scene_->CreateComponent<DebugRenderer>(LOCAL);

	// Create physics world
	PhysicsWorld* physWorld = scene_->CreateComponent<PhysicsWorld>(LOCAL);
	physWorld->SetFps(6);

	Node* terrainNode = scene_->CreateChild("Terrain", LOCAL);   // create a node for the terrain
	terrainNode->SetPosition(Vector3(precisionOffset, 0.0f, 0.0f));

	Terrain* terrain = terrainNode->CreateComponent<Terrain>(LOCAL);
	terrain->SetViewMask(0x00000001);
	terrain->SetSpacing(Vector3(1.4f, 0.24f, 1.4f)*1.0f);
	// the spacing specifies the distance in world units between each heigtmap pixel (XZ)
	// or height value (Y)

	terrain->SetSmoothing(true);

	terrain->SetHeightMap(cache->GetResource<Image>("Textures/HeightMap.png"));
	terrain->SetMaterial(cache->GetResource<Material>("Materials/Terrain.xml"));
	terrain->SetCastShadows(false);
	terrain->SetOccluder(true);
	// the heightmap system can automatically mark objects as invisible that are
	// behind parts of the terrain (like behind a mountain). This improves
	// the rendering performance and is called occluding.

	//RigidBody* terrainBody = terrainNode->CreateComponent<RigidBody>(LOCAL);
	//CollisionShape* terrainShape = terrainNode->CreateComponent<CollisionShape>(LOCAL);
	//terrainShape->SetTerrain();

	// Create a Zone component for ambient lighting & fog control
	Node* zoneNode = scene_->CreateChild("Zone", LOCAL);
	Zone* zone = zoneNode->CreateComponent<Zone>();
	zone->SetBoundingBox(BoundingBox(-7000.0f, 7000.0f));
	zone->SetAmbientColor(Color(0.4f, 0.4f, 0.4f));
	zone->SetFogColor(Color(0.7f, 0.7f, 0.9f));
	zone->SetFogStart(400.0f);
	zone->SetFogEnd(600.0f);

	// Create a directional light to the world. Enable cascaded shadows on it
	Node* lightNode = scene_->CreateChild("DirectionalLight", LOCAL);
	lightNode->SetDirection(Vector3(0.8f, -1.0f, 0.8f));
	Light* light = lightNode->CreateComponent<Light>();
	light->SetLightType(LIGHT_DIRECTIONAL);
	light->SetCastShadows(true);
	light->SetShadowResolution(0.5f);
	light->SetShadowIntensity(0.2f);
	light->SetShadowBias(BiasParameters(0.00025f, 0.5f));
	// Set cascade splits at 10, 50 and 200 world units, fade shadows out at 80% of maximum shadow distance
	light->SetShadowCascade(CascadeParameters(10.0f, 20.0f, 200.0f, 0.0f, 0.8f));

	// Add padding to the navigation mesh in Y-direction so that we can add objects on top of the tallest boxes
	// in the scene and still update the mesh correctly
	//navMesh->SetPadding(Vector3(0.0f, 10.0f, 0.0f));
	// Now build the navigation geometry. This will take some time. Note that the navigation mesh will prefer to use
	// physics geometry from the scene nodes, as it often is simpler, but if it can not find any (like in this example)
	// it will use renderable geometry instead

	/*Node* cylinder = scene_->CreateChild("Box", LOCAL);
	cylinder->SetScale(Vector3(1.0f, 20.0f, 1.0f));
	cylinder->SetPosition(Vector3(23.0f, 3.0f, 23.0f));
	cylinder->CreateComponent<StaticModel>()->SetModel(cache->GetResource<Model>("Models/Cylinder.mdl"));*/

	// Create a NavigationMesh component to the scene root
	NavigationMesh* navMesh = scene_->CreateComponent<NavigationMesh>(LOCAL);

	// Enable drawing  off-mesh connections
	// Set the agent height large enough to exclude the layers under boxes
	navMesh->SetAgentHeight(1.75f);
	// Set nav mesh cell height to minimum (allows agents to be grounded)
	navMesh->SetTileSize(32);
	navMesh->SetCellHeight(0.05f);
	navMesh->SetCellSize(0.5f);
	navMesh->SetAgentMaxSlope(300.0f);
	navMesh->SetAgentMaxClimb(10.0f);

	// Create a Navigable component to the scene root. This tags all of the geometry in the scene as being part of the
	// navigation mesh. By default this is recursive, but the recursion could be turned off from Navigable
	scene_->CreateComponent<Navigable>(LOCAL);

	if (!LoadNavigationData()) {
		navMesh->Build();
		SaveNavigationData();
	}

	// Create a CrowdManager component to the scene root
	CrowdManager* crowdManager = scene_->CreateComponent<CrowdManager>(LOCAL);
	crowdManager->SetMaxAgents(8000);

	CrowdObstacleAvoidanceParams params = crowdManager->GetObstacleAvoidanceParams(0);
	params.velBias = 0.5f;
	params.weightDesVel = 1.0f;
	params.weightCurVel = 0.5f;
	params.weightSide = 0.1f;
	params.weightToi = 2.5f;
	params.horizTime = 2.5f;
	params.gridSize = 33;
	params.adaptiveDivs = 7;
	params.adaptiveRings = 3;
	params.adaptiveDepth = 5;
	// Set the params to "High (66)" setting
	//params.velBias = 0.5f;
	//params.weightCurVel = 0.2f;
	//params.weightDesVel = 0.2f;
	//params.adaptiveDivs = 7;
	//params.adaptiveRings = 3;
	//params.adaptiveDepth = 3;
	//params.weightSide = 50.0f;
	//params.weightToi = 50.0f;
	crowdManager->SetObstacleAvoidanceParams(0, params);

	// Create command applier
	scene_->CreateComponent<CommandApplier>(LOCAL);

	// Create the player
	playerNode_ = scene_->CreateChild("Player", LOCAL);
	
	Camera* camera = playerNode_->CreateComponent<Camera>(LOCAL);
	//camera->SetOrthographic(true);
	//camera->SetOrthoSize(40.0f);
	camera->SetFarClip(1500.0f);

	// Set an initial position for the player scene node above the plane and looking down
	playerNode_->SetPosition(Vector3(precisionOffset, 40.0f, -30.0f));
	pitch_ = 40.0f;//40.0f;
	playerNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));

	playerNode_->CreateComponent<UnitSelector>(LOCAL);
	playerNode_->CreateComponent<UnitController>(LOCAL);

	UnitStats hopliteStats = UnitStats();
	hopliteStats.attack_ = 0.12f;
	hopliteStats.attackDelay_ = 2.5f;
	hopliteStats.defence_ = 0.18f;
	hopliteStats.initiative_ = 2.0f;
	hopliteStats.pushback_ = 2.5f;
	hopliteStats.pushForwards_ = 0.8f;
	hopliteStats.moveToAttackDistance_ = 0.2f;
	hopliteStats.attackDistance_ = 2.0f;

	//hopliteStats.formationRandomness_ = 0.5f;

	UnitStats hastatiStats = UnitStats();
	hastatiStats.attack_ = 0.25f;
	hastatiStats.attackDelay_ = 2.0f;
	hastatiStats.defence_ = 0.10f;
	hastatiStats.initiative_ = 1.0f;
	hastatiStats.pushback_ = 1.2f;
	hastatiStats.pushForwards_ = 1.0f;
	hastatiStats.moveToAttackDistance_ = 3.0f;
	hastatiStats.attackDistance_ = 1.7f;

	hastatiStats.formationRandomness_ = 0.5f;

	for (int i = 0; i < 3; i++) {
		// Create unit nodes
		auto unitNode = scene_->CreateChild("Unit", LOCAL);
		unitNode->SetPosition(Vector3(precisionOffset - 4.0f, 0.0f, (float)i*22.0f));

		unitNode->CreateComponent<LocalFormationCoordinator>(LOCAL);
		Unit* unit = unitNode->CreateComponent<Unit>(LOCAL);

		unit->SetSoldierCount(160);
		unit->SetSoldierModel(HASTATI_MODEL);
		unit->SetSoldierAnimSet(HASTATI_ANIM);
		unit->SetTeam(1);

		SharedPtr<GridFormation> formation(new GridFormation(context_));
		formation->rowSpacing_ = 1.37f;
		formation->columnSpacing_ = 1.37f;

		unit->Spawn(hastatiStats, formation);
	}

	for (int i = 0; i < 3; i++) {
		// Create unit nodes
		auto unitNode = scene_->CreateChild("Unit", LOCAL);
		unitNode->SetPosition(Vector3(precisionOffset + 4.0f, 0.0f, (float)i*22.0f));
		unitNode->SetDirection(-unitNode->GetDirection());

		unitNode->CreateComponent<LocalFormationCoordinator>(LOCAL);
		Unit* unit = unitNode->CreateComponent<Unit>(LOCAL);

		unit->SetSoldierCount(160);
		unit->SetSoldierModel(HOPLITE_MODEL);
		unit->SetSoldierAnimSet(HASTATI_ANIM);
		unit->SetTeam(1);

		SharedPtr<GridFormation> formation(new GridFormation(context_));
		//formation->rowSpacing_ = 1.37f;
		//formation->columnSpacing_ = 1.37f;

		unit->Spawn(hopliteStats, formation);
	}

	/*for (int i = -3; i < 3; i++) {
		// Create unit nodes
		auto unitNode = scene_->CreateChild("Unit", LOCAL);
		unitNode->SetPosition(Vector3(precisionOffset + 30.0f, 0.0f, (float)i*22.0f));

		unitNode->CreateComponent<FormationCoordinator>(LOCAL);
		Unit* unit = unitNode->CreateComponent<Unit>(LOCAL);

		unit->SetSoldierCount(160);
		unit->SetTeam(1);
		unit->Spawn();
	}*/

	/*auto originMarkerNode = scene_->CreateChild("Unit", LOCAL);
	originMarkerNode->SetPosition(Vector3::ZERO);
	auto model = originMarkerNode->CreateComponent<StaticModel>(LOCAL);
	model->SetModel(cache->GetResource<Model>("Models/Cylinder.mdl"));*/

	// Autoconnect to local since I'm lazy
	//scene_->GetComponent<LockstepServer>()->StartServer();
	//GetSubsystem<Network>()->Connect("localhost", SERVER_PORT, scene_);
}

void CrowdNavigation::CreateUI()
{
    ResourceCache* cache = GetSubsystem<ResourceCache>();
    UI* ui = GetSubsystem<UI>();
	UIElement* root = GetSubsystem<UI>()->GetRoot();

    // Create a Cursor UI element because we want to be able to hide and show it at will. When hidden, the mouse cursor will
    // control the camera, and when visible, it will point the raycast target
    XMLFile* style = cache->GetResource<XMLFile>("UI/DefaultStyle.xml");
	// Set style to the UI root so that elements will inherit it
	root->SetDefaultStyle(style);

    SharedPtr<Cursor> cursor(new Cursor(context_));
    cursor->SetStyleAuto(style);
    ui->SetCursor(cursor);

    // Set starting position of the cursor at the rendering window center
    Graphics* graphics = GetSubsystem<Graphics>();
    cursor->SetPosition(graphics->GetWidth() / 2, graphics->GetHeight() / 2);

	buttonContainer_ = root->CreateChild<UIElement>();
	buttonContainer_->SetFixedSize(graphics->GetWidth(), 40);
	buttonContainer_->SetPosition(0, graphics->GetHeight() - 40);
	buttonContainer_->SetLayoutMode(LM_HORIZONTAL);

	serverTextEdit_ = buttonContainer_->CreateChild<LineEdit>();
	serverTextEdit_->SetStyleAuto();
	serverTextEdit_->SetText("localhost");

	connectButton_ = CreateButton("Connect", 100);
	disconnectButton_ = CreateButton("Disconnect", 100);

	startServerButton_ = CreateButton("Start Server", 120);
	stopServerButton_ = CreateButton("Stop Server", 120);

	UpdateButtons();
}

Button* CrowdNavigation::CreateButton(const String& text, int width)
{
	ResourceCache* cache = GetSubsystem<ResourceCache>();
	Font* font = cache->GetResource<Font>("Fonts/Anonymous Pro.ttf");

	Button* button = buttonContainer_->CreateChild<Button>();
	button->SetStyleAuto();
	button->SetFixedWidth(width);

	Text* buttonText = button->CreateChild<Text>();
	buttonText->SetFont(font, 12);
	buttonText->SetAlignment(HA_CENTER, VA_CENTER);
	buttonText->SetText(text);

	return button;
}

void CrowdNavigation::SetupViewport()
{
    Renderer* renderer = GetSubsystem<Renderer>();

    // Set up a viewport to the Renderer subsystem so that the 3D scene can be seen
    SharedPtr<Viewport> viewport(new Viewport(context_, scene_, playerNode_->GetComponent<Camera>()));
    renderer->SetViewport(0, viewport);
}

void CrowdNavigation::SubscribeToEvents()
{
    // Subscribe HandleUpdate() function for processing update events
    SubscribeToEvent(E_UPDATE, URHO3D_HANDLER(CrowdNavigation, HandleUpdate));

	SubscribeToEvent(E_PHYSICSPOSTSTEP, URHO3D_HANDLER(CrowdNavigation, HandlePhysicsPostStep));

	SubscribeToEvent(connectButton_, E_RELEASED, URHO3D_HANDLER(CrowdNavigation, HandleConnect));
	SubscribeToEvent(disconnectButton_, E_RELEASED, URHO3D_HANDLER(CrowdNavigation, HandleDisconnect));

	SubscribeToEvent(startServerButton_, E_RELEASED, URHO3D_HANDLER(CrowdNavigation, HandleStartServer));
	SubscribeToEvent(stopServerButton_, E_RELEASED, URHO3D_HANDLER(CrowdNavigation, HandleStopServer));

	SubscribeToEvent(E_SERVERCONNECTED, URHO3D_HANDLER(CrowdNavigation, HandleConnectionStatus));
	SubscribeToEvent(E_SERVERDISCONNECTED, URHO3D_HANDLER(CrowdNavigation, HandleConnectionStatus));
	SubscribeToEvent(E_CONNECTFAILED, URHO3D_HANDLER(CrowdNavigation, HandleConnectionStatus));
	SubscribeToEvent(E_NETWORKMESSAGE, URHO3D_HANDLER(CrowdNavigation, HandleNetworkMessage));

    // Subscribe HandlePostRenderUpdate() function for processing the post-render update event, during which we request debug geometry
    SubscribeToEvent(E_POSTRENDERUPDATE, URHO3D_HANDLER(CrowdNavigation, HandlePostRenderUpdate));

	SubscribeToEvent(E_STEP, URHO3D_HANDLER(CrowdNavigation, HandleSimStep));
	GetSubsystem<Network>()->RegisterRemoteEvent(E_STEP);
}

void CrowdNavigation::MoveCamera(float timeStep)
{
    // Right mouse button controls mouse cursor visibility: hide when pressed
    UI* ui = GetSubsystem<UI>();
    Input* input = GetSubsystem<Input>();
    ui->GetCursor()->SetVisible(!input->GetMouseButtonDown(MOUSEB_MIDDLE));

    // Do not move if the UI has a focused element (the console)
    if (ui->GetFocusElement())
        return;

    // Movement speed as world units per second
    float moveSpeed = 30.0f;
    // Mouse sensitivity as degrees per pixel
    const float MOUSE_SENSITIVITY = 0.1f;

	if (input->GetKeyDown(KEY_SHIFT)) {
		moveSpeed *= 4.0f;
	}

    // Use this frame's mouse motion to adjust camera node yaw and pitch. Clamp the pitch between -90 and 90 degrees
    // Only move the camera when the cursor is hidden
    if (!ui->GetCursor()->IsVisible())
    {
        IntVector2 mouseMove = input->GetMouseMove();
        yaw_ += MOUSE_SENSITIVITY * mouseMove.x_;
        pitch_ += MOUSE_SENSITIVITY * mouseMove.y_;
        pitch_ = Clamp(pitch_, -90.0f, 90.0f);

        // Construct new orientation for the camera scene node from yaw and pitch. Roll is fixed to zero
		playerNode_->SetRotation(Quaternion(pitch_, yaw_, 0.0f));
    }

    // Read WASD keys and move the camera scene node to the corresponding direction if they are pressed
    if (input->GetKeyDown(KEY_W))
		playerNode_->Translate(Vector3::FORWARD * moveSpeed * timeStep);
    if (input->GetKeyDown(KEY_S))
		playerNode_->Translate(Vector3::BACK * moveSpeed * timeStep);
    if (input->GetKeyDown(KEY_A))
		playerNode_->Translate(Vector3::LEFT * moveSpeed * timeStep);
    if (input->GetKeyDown(KEY_D))
		playerNode_->Translate(Vector3::RIGHT * moveSpeed * timeStep);

    // Check for loading/saving the scene from/to the file Data/Scenes/CrowdNavigation.xml relative to the executable directory
    if (input->GetKeyPress(KEY_F5))
    {
        File saveFile(context_, GetSubsystem<FileSystem>()->GetProgramDir() + "Data/Scenes/CrowdNavigation.xml", FILE_WRITE);
        scene_->SaveXML(saveFile);
    }
    else if (input->GetKeyPress(KEY_F7))
    {
        File loadFile(context_, GetSubsystem<FileSystem>()->GetProgramDir() + "Data/Scenes/CrowdNavigation.xml", FILE_READ);
        scene_->LoadXML(loadFile);
    }

    // Toggle debug geometry with space
    else if (input->GetKeyPress(KEY_SPACE))
        drawDebug_ = !drawDebug_;
}

bool CrowdNavigation::LoadNavigationData()
{
	File loadFile(context_);
	
	if (loadFile.Open(GetSubsystem<FileSystem>()->GetProgramDir() + "NavMesh.bin", FILE_READ)) {
		NavigationMesh* navMesh = scene_->GetComponent<NavigationMesh>();
		auto data = loadFile.ReadBuffer();
		navMesh->SetNavigationDataAttr(data);
		loadFile.Close();
		return true;
	} else {
		return false;
	}
}

void CrowdNavigation::SaveNavigationData()
{
	NavigationMesh* navMesh = scene_->GetComponent<NavigationMesh>();
	PODVector<unsigned char> navData = navMesh->GetNavigationDataAttr();
	
	File saveFile(context_, GetSubsystem<FileSystem>()->GetProgramDir() + "NavMesh.bin", FILE_WRITE);
	auto data = navMesh->GetNavigationDataAttr();
	saveFile.WriteBuffer(data);
	saveFile.Flush();
	saveFile.Close();
}

void CrowdNavigation::HandlePhysicsPostStep(StringHash eventType, VariantMap& eventData)
{
	using namespace PhysicsPostStep;

	localPhysicsTick_++;

	// Apply commands for next frame
	scene_->GetComponent<CommandApplier>()->ApplyCommands(localPhysicsTick_ + 1);

	// Update server on our status
	Network* network = GetSubsystem<Network>();
	Connection* serverConnection = network->GetServerConnection();

	VariantMap remoteEventData;
	remoteEventData[P_TICK] = localPhysicsTick_;
	remoteEventData[P_CHECKSUM] = CalculateChecksum();

	if (serverConnection)
		serverConnection->SendRemoteEvent(E_CLIENTSTEP, true, remoteEventData);

	if (localPhysicsTick_ >= serverPhysicsTick_) {
		// We are about to get ahead of the server, stop!

		// Set the timescale to 0 so another physics frame won't happen until we let it (by re-increasing the timescale)
		scene_->SetTimeScale(0.0f);
	}
}

int CrowdNavigation::CalculateChecksum() {
	URHO3D_PROFILE(CalculateChecksum);

	auto units = PODVector<Node*>();
	scene_->GetChildrenWithComponent<Unit>(units);

	int checksum = 0;

	for (unsigned i = 0; i < units.Size(); i++) {
		Unit* unit = units[i]->GetComponent<Unit>();

		Vector3 position = unit->GetPosition();

		// interpret raw bit data as integer and add onto checksum
		checksum += *((int*)& position.x_) + *((int*)& position.y_) * *((int*)& position.z_);
	}

	return checksum;
}

/*int CrowdNavigation::CalculateChecksum() {
	URHO3D_PROFILE(CalculateChecksum);

	auto agents = scene_->GetComponent<CrowdManager>()->GetAgents();;

	int checksum = 0;

	for (unsigned i = 0; i < agents.Size(); i++) {
		CrowdAgent* ag = agents[i];

		Vector3 position = ag->GetPosition();

		// interpret raw bit data as integer and add onto checksum
		checksum += *((int*)& position.x_) + *((int*)& position.y_) * *((int*)& position.z_);
	}

	return checksum;
}*/

void CrowdNavigation::HandleUpdate(StringHash eventType, VariantMap& eventData)
{
    using namespace Update;

    // Take the frame time step, which is stored as a float
    float timeStep = eventData[P_TIMESTEP].GetFloat();

	// Move the camera, scale movement with time step
	MoveCamera(timeStep);
}

void CrowdNavigation::HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData)
{
    if (drawDebug_)
    {
        // Visualize navigation mesh, obstacles and off-mesh connections
        scene_->GetComponent<NavigationMesh>()->DrawDebugGeometry(true);
        // Visualize agents' path and position to reach
        scene_->GetComponent<CrowdManager>()->DrawDebugGeometry(true);
		// Visualize physics world
		scene_->GetComponent<PhysicsWorld>()->DrawDebugGeometry(true);
    }
}

void CrowdNavigation::HandleConnect(StringHash eventType, VariantMap& eventData)
{
	Network* network = GetSubsystem<Network>();
	String address = serverTextEdit_->GetText().Trimmed();
	if (address.Empty())
		address = "localhost"; // Use localhost to connect if nothing else specified

	network->Connect(address, SERVER_PORT, scene_);

	UpdateButtons();
}

void CrowdNavigation::HandleDisconnect(StringHash eventType, VariantMap& eventData)
{
	Network* network = GetSubsystem<Network>();
	Connection* serverConnection = network->GetServerConnection();
	if (serverConnection)
	{
		serverConnection->Disconnect();
		localPhysicsTick_ = 0;
		//scene_->Clear(true, false);
	}

	UpdateButtons();
}

void CrowdNavigation::HandleStartServer(StringHash eventType, VariantMap & eventData)
{
	scene_->GetComponent<LockstepServer>()->StartServer();

	Network* network = GetSubsystem<Network>();
	// And connect to our own server
	network->Connect("localhost", SERVER_PORT, scene_);

	UpdateButtons();
}

void CrowdNavigation::HandleStopServer(StringHash eventType, VariantMap & eventData)
{
	scene_->GetComponent<LockstepServer>()->StopServer();
	UpdateButtons();
}

void CrowdNavigation::HandleNetworkMessage(StringHash eventType, VariantMap& eventData)
{
	using namespace NetworkMessage;

	if (eventData[P_MESSAGEID].GetInt() == M_CLIENTCOMMAND) {
		VectorBuffer buf = eventData[P_DATA].GetBuffer();

		Command* command;
		CommandType type = (CommandType)buf.ReadInt();
		COMMAND_CREATETYPE(command, type);

		auto bufData = buf.GetData();
		int commandBufferChecksum = 0;
		for (unsigned i = 0; i < buf.GetSize(); i++) {
			commandBufferChecksum += bufData[i];
		}
		Log::Write(1, "Command checksum: " + String(commandBufferChecksum));

		command->Load(buf);

		if (command->GetApplicationTick() <= localPhysicsTick_) {
			URHO3D_LOGERROR("Command " + String(commandBufferChecksum) + " happened in past! It has not been applied.");
			delete command;
			return;
		}

		scene_->GetComponent<CommandApplier>()->AddCommand(command);
	}
}

void CrowdNavigation::UpdateButtons()
{
	Network* network = GetSubsystem<Network>();

	Connection* serverConnection = network->GetServerConnection();
	bool isServerRunning = network->IsServerRunning();

	// Show and hide buttons so that eg. Connect and Disconnect are never shown at the same time
	connectButton_->SetVisible(!isServerRunning && serverConnection == nullptr);
	disconnectButton_->SetVisible(!isServerRunning && serverConnection != nullptr);

	startServerButton_->SetVisible(!isServerRunning && serverConnection == nullptr);
	stopServerButton_->SetVisible(isServerRunning);
}

void CrowdNavigation::HandleConnectionStatus(StringHash eventType, VariantMap& eventData)
{
	UpdateButtons();
}

void CrowdNavigation::HandleSimStep(StringHash eventType, VariantMap& eventData) {
	serverPhysicsTick_ = eventData[P_TICK].GetInt();

	if (serverPhysicsTick_ > localPhysicsTick_) {
		scene_->SetTimeScale(timeScale_);
	}
}