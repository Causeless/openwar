#include "Formation.h"
#include "VecUtils.h"

#include <Windows.h>

#include <Urho3D/Core/Context.h>
#include <Urho3D/Math/Ray.h>
#include <Urho3D/Scene/Serializable.h>
#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Graphics/DebugRenderer.h>

#include <Urho3D/IO/Log.h>

//const Formation Formation::NONE = 0;

const char* formationSpotStructureElementNames[] =
{
	"Formation Spot Count",
	"   Position",
	"   Direction",
	0
};

Formation::Formation(Context* context) : Serializable(context) {

}

void Formation::RegisterObject(Context* context)
{
	context->RegisterFactory<Formation>("FORMATION");

	URHO3D_ATTRIBUTE("Type", int, type_, FORMATION_NONE, AM_DEFAULT);
	URHO3D_ATTRIBUTE("File Count", unsigned, fileCount_, 0, AM_DEFAULT);
	URHO3D_ATTRIBUTE("Rank Count", unsigned, rankCount_, 0, AM_DEFAULT);
	URHO3D_ATTRIBUTE("Start Pos", Vector3, startPoint_, Vector3::ZERO, AM_DEFAULT);
	URHO3D_ATTRIBUTE("End Pos", Vector3, endPoint_, Vector3::ZERO, AM_DEFAULT);
	URHO3D_ATTRIBUTE("Centre", Vector3, centre_, Vector3::ZERO, AM_DEFAULT);
	URHO3D_MIXED_ACCESSOR_VARIANT_VECTOR_STRUCTURE_ATTRIBUTE("Formation Spots", GetFormationSpotsAttr, SetFormationSpotsAttr,
		VariantVector, Variant::emptyVariantVector,
		formationSpotStructureElementNames, AM_DEFAULT);
}

VariantVector Formation::GetFormationSpotsAttr() const
{
	auto ret = VariantVector();

	ret.Reserve(1);
	ret.Push(formationSpots_.Size());

	return ret;
}

void Formation::SetFormationSpotsAttr(const VariantVector& value)
{
	formationSpots_.Clear();

	unsigned index = 0;
	unsigned numFormationSpots = index < value.Size() ? value[index++].GetUInt() : 0;

	Set(startPoint_, endPoint_, numFormationSpots);
}

// Utility function because I'm too lazy to make a proper efficient way of doing this just now
void Formation::AutoGenConnections(float radius) 
{
	float radiusSquared = radius * radius;

	for (unsigned i = 0; i < formationSpots_.Size(); i++) {
		FormationSpot* spot = &formationSpots_[i];

		// Clear to no connections
		for (unsigned j = 0; j < MAX_FORMATION_SPOT_CONNECTIONS; j++) {
			spot->connections[j] = spot->id;
		}

		unsigned currConnections = 0;

		// Now find up to 4 other spots within radius and connect to them
		for (unsigned j = 0; j < formationSpots_.Size(); j++) {
			FormationSpot* otherSpot = &formationSpots_[j];

			if (spot == otherSpot) {
				continue;
			}

			if (currConnections >= MAX_FORMATION_SPOT_CONNECTIONS) {
				break;
			}

			if ((spot->position - otherSpot->position).LengthSquared() <= radiusSquared) {
				spot->connections[currConnections] = otherSpot->id;
				currConnections++;
			}
		}
	}
}

void Formation::Set(Vector3 startPoint, Vector3 endPoint, unsigned count) 
{
	startPoint_ = startPoint;
	endPoint_ = endPoint;
}

// Clone the details of a formation, but not the actual formation! I.e grid spacing and stuff. If you want the actual formation, call set() on the copy with startpoint and endpoint afterwards
SharedPtr<Formation> Formation::CloneDetails() const
{
	SharedPtr<Formation> formation(new Formation(context_));
	formation->startPoint_ = startPoint_;
	formation->endPoint_ = endPoint_;
	return formation;
}

void Formation::RecalculateCentre()
{
	centre_ = Vector3::ZERO;

	for (unsigned i = 0; i < formationSpots_.Size(); i++) {
		centre_ += formationSpots_[i].position;
	}

	centre_ /= (float)formationSpots_.Size();

	dirty_ = false;
}

Vector3 Formation::GetCentre()
{
	if (dirty_) {
		RecalculateCentre();
	}

	return centre_;
}

Vector3 Formation::GetFront()
{
	return (startPoint_ + endPoint_) * 0.5f;
}

const PODVector<FormationSpot>& Formation::GetSpots()
{
	return formationSpots_;
}

FormationSpot Formation::GetSpot(IntVector2 pos)
{
	auto file = GetFile(pos.x_);
	return file[pos.y_];
}

PODVector<FormationSpot> Formation::GetFile(int file)
{
	PODVector<FormationSpot> fileSpots = PODVector<FormationSpot>();
	fileSpots.Reserve(rankCount_);

	for (auto spot : formationSpots_) {
		if (spot.fileAndRank.x_ == file) {
			fileSpots.Push(spot);
		}
	}

	return fileSpots;
}

PODVector<FormationSpot> Formation::GetRank(int rank)
{
	PODVector<FormationSpot> rankSpots = PODVector<FormationSpot>();
	rankSpots.Reserve(fileCount_);

	for (auto spot : formationSpots_) {
		if (spot.fileAndRank.y_ == rank) {
			rankSpots.Push(spot);
		}
	}

	return rankSpots;
}

GridFormation::GridFormation(Context* context) : Formation(context) {
	type_ = FORMATION_GRID;
}

void GridFormation::RegisterObject(Context* context)
{
	context->RegisterFactory<GridFormation>("FORMATION");

	// Make sure these are before the base attritubtes, not after, as formation spots from Set() depend on these.
	URHO3D_ATTRIBUTE("Column Spacing", float, columnSpacing_, 0.0f, AM_DEFAULT);
	URHO3D_ATTRIBUTE("Row Spacing", float, rowSpacing_, 0.0f, AM_DEFAULT);
	URHO3D_COPY_BASE_ATTRIBUTES(Formation);
}

SharedPtr<Formation> GridFormation::CloneDetails() const
{
	SharedPtr<GridFormation> formation(new GridFormation(context_));
	formation->startPoint_ = startPoint_;
	formation->endPoint_ = endPoint_;
	formation->columnSpacing_ = columnSpacing_;
	formation->rowSpacing_ = rowSpacing_;
	return formation;
}

void GridFormation::Set(Vector3 startPoint, Vector3 endPoint, unsigned spotsCount)
{
	Formation::Set(startPoint, endPoint, spotsCount);

	// Pre-allocate vector
	formationSpots_ = PODVector<FormationSpot>();
	formationSpots_.Reserve(spotsCount);

	if (spotsCount == 0) {
		return;
	}

	Vector3 directionAlongFront = (endPoint - startPoint).Normalized();

	Vector3 backwards = Vector3::UP.CrossProduct(directionAlongFront);
	Vector3 forwards = backwards * -1;

	float length = (endPoint - startPoint).Length();
	unsigned spotsPerRow = (unsigned)roundf(length / columnSpacing_);

	float realLength = columnSpacing_ * (float)spotsPerRow;
	float difference = length - realLength;

	// Offset so that the gap is equally distributed at the left and right edges of the formation, not just one side
	Vector3 offset = (directionAlongFront * difference*0.5f) + (directionAlongFront*columnSpacing_*0.75f);

	unsigned row = 0;
	unsigned id = 0;

	// While there are enough soldiers left for another complete row
	while (spotsCount >= spotsPerRow) 
	{
		for (unsigned column = 0; column < spotsPerRow; column++)
		{
			spotsCount--;

			Vector3 currentPos = startPoint + offset;
			currentPos += directionAlongFront * columnSpacing_ * (float)column; // Add column spacing
			currentPos += backwards * rowSpacing_ * (float)row; // Add row spacing

			FormationSpot currentSpot = FormationSpot();
			currentSpot.id = id++;
			currentSpot.fileAndRank = IntVector2(column, row);
			currentSpot.position = currentPos;
			currentSpot.direction = forwards;

			formationSpots_.Push(currentSpot);
		}
		row++;
	}

	// Now we must put the rest of the soldiers on the back row

	// Used to centre back row
	float backrowSlideAmount = (float)(spotsPerRow - spotsCount) * 0.5f;
	Vector3 backrowSlide = directionAlongFront * backrowSlideAmount * columnSpacing_;

	for (unsigned column = 0; column < spotsCount; column++)
	{
		Vector3 currentPos = startPoint + offset;
		currentPos += (directionAlongFront * columnSpacing_ * (float)column) + backrowSlide; // Add column spacing
		currentPos += backwards * rowSpacing_ * (float)row; // Add row spacing

		FormationSpot currentSpot = FormationSpot();
		currentSpot.id = id++;
		currentSpot.fileAndRank = IntVector2(column, row);
		currentSpot.position = currentPos;
		currentSpot.direction = forwards;

		formationSpots_.Push(currentSpot);
	}

	rankCount_ = row + 1;
	fileCount_ = spotsPerRow;

	AutoGenConnections(max(rowSpacing_, columnSpacing_)*1.3f + 0.1f /*Epsilon*/);

	RecalculateCentre();
}

HexFormation::HexFormation(Context* context) : Formation(context) {
	type_ = FORMATION_HEX;
}

void HexFormation::RegisterObject(Context* context)
{
	context->RegisterFactory<HexFormation>("FORMATION");

	// Make sure these are before the base attritubtes, not after, as formation spots from Set() depend on these.
	URHO3D_ATTRIBUTE("Column Spacing", float, columnSpacing_, 0.0f, AM_DEFAULT);
	URHO3D_ATTRIBUTE("Row Spacing", float, rowSpacing_, 0.0f, AM_DEFAULT);
	URHO3D_COPY_BASE_ATTRIBUTES(Formation);
}

void HexFormation::Set(Vector3 startPoint, Vector3 endPoint, unsigned spotsCount)
{
	Formation::Set(startPoint, endPoint, spotsCount);

	// Pre-allocate vector
	formationSpots_ = PODVector<FormationSpot>();
	formationSpots_.Reserve(spotsCount);

	Vector3 directionAlongFront = (endPoint - startPoint).Normalized();

	Vector3 backwards = Vector3::UP.CrossProduct(directionAlongFront);
	Vector3 forwards = backwards * -1;

	float length = (endPoint - startPoint).Length();
	unsigned spotsPerRow = (unsigned)roundf(length / columnSpacing_);

	float realLength = columnSpacing_ * (float)spotsPerRow;
	float difference = length - realLength;

	// Offset so that the gap is equally distributed at the left and right edges of the formation, not just one side
	Vector3 offset = (directionAlongFront * difference*0.5f) + (directionAlongFront*columnSpacing_*0.75f);

	unsigned row = 0;
	unsigned id = 0;

	// While there are enough soldiers left for another complete row
	while (spotsCount >= spotsPerRow)
	{
		unsigned spotsForThisRow = row % 2 == 0 ? spotsPerRow : spotsPerRow - 1;
		for (unsigned column = 0; column < spotsForThisRow; column++)
		{
			spotsCount--;

			Vector3 currentPos = startPoint + offset;

			currentPos += row % 2 == 0 ? 
				  (directionAlongFront * columnSpacing_ * (float)column)  // Add column spacing - normal
				: (directionAlongFront * columnSpacing_ * (float)column) + (directionAlongFront * columnSpacing_ * 0.5f); // Add column spacing - in-between rows

			currentPos += backwards * rowSpacing_ * (float)row; // Add row spacing

			FormationSpot currentSpot = FormationSpot();
			currentSpot.id = id++;
			currentSpot.fileAndRank = IntVector2(column, row);
			currentSpot.position = currentPos;
			currentSpot.direction = forwards;

			formationSpots_.Push(currentSpot);
		}
		row++;
	}

	// Now we must put the rest of the soldiers on the back row

	// Used to centre back row
	float backrowSlideAmount = (float)(spotsPerRow - spotsCount) * 0.5f;
	Vector3 backrowSlide = directionAlongFront * backrowSlideAmount * columnSpacing_;

	for (unsigned column = 0; column < spotsCount; column++)
	{
		Vector3 currentPos = startPoint + offset;
		currentPos += (directionAlongFront * columnSpacing_ * (float)column) + backrowSlide; // Add column spacing
		currentPos += backwards * rowSpacing_ * (float)row; // Add row spacing

		FormationSpot currentSpot = FormationSpot();
		currentSpot.id = id++;
		currentSpot.fileAndRank = IntVector2(column, row);
		currentSpot.position = currentPos;
		currentSpot.direction = forwards;

		formationSpots_.Push(currentSpot);
	}

	rankCount_ = row + 1;
	fileCount_ = spotsPerRow;

	AutoGenConnections(max(rowSpacing_, columnSpacing_)*1.3f + 0.1f /*Epsilon*/);

	RecalculateCentre();
}

SharedPtr<Formation> HexFormation::CloneDetails() const
{
	SharedPtr<HexFormation> formation(new HexFormation(context_));
	formation->startPoint_ = startPoint_;
	formation->endPoint_ = endPoint_;
	formation->columnSpacing_ = columnSpacing_;
	formation->rowSpacing_ = rowSpacing_;
	return formation;
}