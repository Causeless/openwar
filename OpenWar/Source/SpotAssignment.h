#pragma once

#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Scene/Node.h>
#include "Formation.h"


using namespace Urho3D;

class Unit;

// SpotAssignment handles assigning soldiers to slots so they move into formations sensibly
class SpotAssignment {
public:
	int GetSpotForSoldier(Node* soldier);
	Node* GetSoldierForSpot(int spotId);

	void ImproveSpotAssignment(Unit& unit, Formation& formation);
protected:
	void SetSpotForSoldier(Node* soldier, int spotId);
	void SwapSoldierAssignments(Node* soldier1, Node* soldier2);

	// Hash of soldier nodes to formation spot ids
	HashMap<Node*, int> soldiersToSpots_;
	// and inverse
	HashMap<int, Node*> spotsToSoldiers_;
};


class IDSpotAssignment : public SpotAssignment {
public:
	IDSpotAssignment(Unit& unit, Formation& formation);
};

class HungarianAlgorithmSpotAssignment : public SpotAssignment {
public:
	HungarianAlgorithmSpotAssignment(Unit& unit, Formation& formation);
};