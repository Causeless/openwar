#include "Command.h"
#include "Formation.h"

#include "Urho3D/Core/WorkQueue.h"
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Core/Context.h>
#include <Urho3D/IO/Log.h>

void ApplyCommandsWork(const WorkItem* item, unsigned threadIndex) {
	Scene* scene = reinterpret_cast<Scene*>(item->aux_);
	Command** start = reinterpret_cast<Command**>(item->start_);
	Command** end = reinterpret_cast<Command**>(item->end_);

	while (start != end)
	{
		Command* command = *start;
		if (command) {
			command->Apply(scene);
		}
		++start;
	}
}

CommandApplier::CommandApplier(Context* context) : Component(context) {
}

CommandApplier::~CommandApplier() {
	auto allCommands = commandsPerTick_.Values();

	for (unsigned i = 0; i < allCommands.Size(); i++)
		for (unsigned j = 0; j < allCommands[i].Size(); j++)
			delete allCommands[i][j];
}

void CommandApplier::RegisterObject(Context* context)
{
	context->RegisterFactory<CommandApplier>("COMMAND_APPLIER");
}

void CommandApplier::OnNodeSet(Node* node) {
	commandsPerTick_ = HashMap<int, Vector<Command*>>();
}

void CommandApplier::AddCommand(Command* command)
{
	int tick = command->tick_;

	Vector<Command*> commandsThisTick;
	if (commandsPerTick_.Contains(tick)) {
		commandsThisTick = commandsPerTick_[tick];
	}

	commandsThisTick.Push(command);
	commandsPerTick_[tick] = commandsThisTick;
}

void CommandApplier::ApplyCommands(int tick)
{
	if (!commandsPerTick_.Contains(tick))
		return;

	auto commands = commandsPerTick_[tick];

	Scene* scene = node_->GetScene();
	WorkQueue* queue = GetSubsystem<WorkQueue>();
	scene->BeginThreadedUpdate();

	int numWorkItems = queue->GetNumThreads() + 1; // Worker threads + main thread
	int commandsPerItem = Max((int)(commands.Size() / numWorkItems), 1);

	PODVector<Command*>::Iterator start = commands.Begin();
	// Create a work item for each thread
	for (int i = 0; i < numWorkItems; ++i)
	{
		SharedPtr<WorkItem> item = queue->GetFreeItem();
		item->priority_ = M_MAX_UNSIGNED;
		item->workFunction_ = ApplyCommandsWork;
		item->aux_ = scene;

		PODVector<Command*>::Iterator end = commands.End();
		if (i < numWorkItems - 1 && end - start > commandsPerItem) {
			end = start + commandsPerItem;
		}

		item->start_ = &(*start);
		item->end_ = &(*end);
		queue->AddWorkItem(item);

		start = end;
	}

	queue->Complete(M_MAX_UNSIGNED);
	scene->EndThreadedUpdate();

	for (unsigned i = 0; i < commands.Size(); i++) {
		Command* command = commands[i];
		Log::Write(1, String("Applied command on tick: ") + String(tick));
		delete command;
	}

	commandsPerTick_.Erase(tick);
}

Command::Command(Context* context) : Serializable(context) {

}

void Command::RegisterObject(Context* context)
{
	URHO3D_ATTRIBUTE("Unit ID", int, unitId_, 0, AM_DEFAULT | AM_COMPONENTID);
	URHO3D_ATTRIBUTE("Tick", int, tick_, 0, AM_DEFAULT);
}

int Command::GetApplicationTick() const
{
	return tick_;
}

void Command::SetApplicationTick(int tick)
{
	tick_ = tick;
}

MovementCommand::MovementCommand(Context* context) : Command(context) {

}

void MovementCommand::RegisterObject(Context* context)
{
	context->RegisterFactory<MovementCommand>("COMMAND");

	URHO3D_COPY_BASE_ATTRIBUTES(Command);
	URHO3D_ATTRIBUTE("Formation", PODVector<unsigned char>, formationBuf_, 0, AM_DEFAULT);
}

void MovementCommand::Set(Unit* unit, Formation &formation) {
	unitId_ = unit->GetID();

	VectorBuffer buf = VectorBuffer();
	buf.WriteInt(formation.GetFormationType());
	formation.Save(buf);

	formationBuf_ = buf.GetBuffer();
}

void MovementCommand::Apply(Scene* scene) {
	Unit* unit = (Unit*)scene->GetComponent(unitId_);

	VectorBuffer buf(formationBuf_);

	SharedPtr<Formation> formation;
	FormationType type = (FormationType)buf.ReadInt();
	FORMATION_CREATETYPE(formation, type, context_);

	formation->Load(buf);

	unit->Attack(nullptr);
	unit->GetCoordinator()->SetFormation(formation);
}

AttackCommand::AttackCommand(Context* context) : Command(context) {

}

void AttackCommand::RegisterObject(Context* context)
{
	context->RegisterFactory<AttackCommand>("COMMAND");

	URHO3D_COPY_BASE_ATTRIBUTES(Command);
	URHO3D_ATTRIBUTE("Target", int, targetId_, 0, AM_DEFAULT);
}

void AttackCommand::Set(Unit* unit, Unit* target) {
	unitId_ = unit->GetID();
	targetId_ = target->GetID();
}

void AttackCommand::Apply(Scene* scene) {
	Unit* unit = (Unit*)scene->GetComponent(unitId_);
	Unit* target = (Unit*)scene->GetComponent(targetId_);

	unit->Attack(target->GetNode());
}