#pragma once

#include <Urho3D/Engine/Engine.h>
#include <Urho3D/Container/Vector.h>

#include "Unit.h"

using namespace Urho3D;

#define COMMAND_CREATETYPE(COMMAND, TYPE) switch (TYPE) { \
case COMMAND_MOVEMENT:                                    \
	COMMAND = new MovementCommand(context_);              \
    break;                                                \
case COMMAND_ATTACK:                                      \
	COMMAND = new AttackCommand(context_);                \
	break;                                                \
default:                                                  \
	COMMAND = new Command(context_);                      \
	break;                                                \
}

enum CommandType: int {
	COMMAND_NONE,
	COMMAND_MOVEMENT,
	COMMAND_ATTACK,
};

static const int M_SERVERCOMMAND = 30;
static const int M_CLIENTCOMMAND = 31;

class Command : public Serializable {
	URHO3D_OBJECT(Command, Serializable);

public:
	friend class CommandApplier;

	Command(Context* contect);
	static void RegisterObject(Context* context);

	int GetApplicationTick() const;
	void SetApplicationTick(int tick);

	virtual void Apply(Scene* scene) {};

protected:
	// The unit to apply the command to
	int unitId_;

	// The tick this command is to be applied
	int tick_;
};

class MovementCommand : public Command {
	URHO3D_OBJECT(MovementCommand, Command);

public:
	MovementCommand(Context* context);
	static void RegisterObject(Context* context);

	void Set(Unit* unit, Formation& formation);
	virtual void Apply(Scene* scene);

private:
	PODVector<unsigned char> formationBuf_;
};

class AttackCommand : public Command {
	URHO3D_OBJECT(AttackCommand, Command);

public:
	AttackCommand(Context* context);
	static void RegisterObject(Context* context);

	void Set(Unit* unit, Unit* target);
	virtual void Apply(Scene* scene);

private:
	int targetId_;
};

class CommandApplier : public Component {
	URHO3D_OBJECT(CommandApplier, Component);

public:
	CommandApplier(Context* context);
	~CommandApplier();

	static void RegisterObject(Context* context);

	// Applies commands on the tick they need to be applied
	void AddCommand(Command* command);
	void ApplyCommands(int tick);

protected:
	/// Handle node being assigned.
	virtual void OnNodeSet(Node* node);

private:
	HashMap<int, Vector<Command*>> commandsPerTick_;
};