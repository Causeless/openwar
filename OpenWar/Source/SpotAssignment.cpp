#include "SpotAssignment.h"
#include "VecUtils.h"
#include "Formation.h"
#include "Unit.h"

#include <Urho3D/Navigation/CrowdAgent.h>
#include <Urho3D/IO/Log.h>

#include "Hungarian.h"
#include <vector> // For hungarian API

int SpotAssignment::GetSpotForSoldier(Node* soldier)
{
	return soldiersToSpots_[soldier];
}

Node* SpotAssignment::GetSoldierForSpot(int spotId)
{
	return spotsToSoldiers_[spotId];
}

void SpotAssignment::SetSpotForSoldier(Node* soldier, int spotId)
{
	soldiersToSpots_[soldier] = spotId;
	spotsToSoldiers_[spotId] = soldier;
}

void SpotAssignment::SwapSoldierAssignments(Node* soldier1, Node* soldier2)
{
	int soldier1_oldSpot = GetSpotForSoldier(soldier1);
	int soldier2_oldSpot = GetSpotForSoldier(soldier2);
	SetSpotForSoldier(soldier1, soldier2_oldSpot);
	SetSpotForSoldier(soldier2, soldier1_oldSpot);
}

IDSpotAssignment::IDSpotAssignment(Unit& unit, Formation& formation)
{
	for (unsigned i = 0; i < formation.GetSpotCount(); i++) {
		Node* soldierNode = unit.GetNode()->GetChild(i);
		SetSpotForSoldier(soldierNode, i);
	}
}

HungarianAlgorithmSpotAssignment::HungarianAlgorithmSpotAssignment(Unit& unit, Formation& formation)
{
	if (unit.GetSoldiers().Size() < 1)
		return;

	auto costMatrix = new vector<vector<float>>();
	vector<int> assignment;

	for (auto soldierNode : unit.GetSoldiers()) {
		CrowdAgent* currentSoldier = soldierNode->GetComponent<CrowdAgent>();
		vector<float> currentSoldierCosts;

		for (unsigned j = 0; j < formation.GetSpotCount(); j++) {
			float currentDistance = (formation.GetSpot(j).position - currentSoldier->GetPosition()).LengthSquared();
			currentSoldierCosts.push_back(currentDistance);
		}

		costMatrix->push_back(currentSoldierCosts);
	}

	HungarianAlgorithm algo;
	algo.Solve(*costMatrix, assignment);

	for (unsigned i = 0; i < unit.GetSoldiers().Size(); i++) {
		Node* soldierNode = unit.GetSoldier(i);
		SetSpotForSoldier(soldierNode, assignment[i]);
	}

	delete costMatrix;
}

void SpotAssignment::ImproveSpotAssignment(Unit& unit, Formation& formation)
{
	if (unit.GetSoldiers().Size() < 1)
		return;

	// First find the most constrained agent (those furthest from their goal) and allow them to improve their assignment
	Node* mostConstrainedSoldier;
	CrowdAgent* mostConstrainedSoldierAgent;
	float maxLength = 0;

	for (auto soldier : unit.GetSoldiers()) {
		CrowdAgent* agent = soldier->GetComponent<CrowdAgent>();
		FormationSpot spot = formation.GetSpot(GetSpotForSoldier(soldier));

		float length = (agent->GetPosition() - spot.position).LengthSquared();

		if (length > maxLength) {
			mostConstrainedSoldier = soldier;
			mostConstrainedSoldierAgent = agent;
			maxLength = length;
			Log::Write(1, "mostConstrainedSoldier length: " + String(length));
		}
	}

	if (maxLength < 1.0f) return;

	int closestSpot;
	float closestSpotDistance = INFINITY;
	for (unsigned i = 0; i < formation.GetSpotCount(); i++) {
		FormationSpot spot = formation.GetSpot(i);
		float spotDistance = (mostConstrainedSoldierAgent->GetPosition() - spot.position).LengthSquared();

		if (spotDistance < closestSpotDistance) {
			closestSpot = i;
			closestSpotDistance = spotDistance;
			Log::Write(1, "closestSpotDistance: " + String(closestSpotDistance));
		}
	}

	Node* otherSoldier = GetSoldierForSpot(closestSpot);
	SwapSoldierAssignments(mostConstrainedSoldier, otherSoldier);
}